gulp = require 'gulp'
coffee = require 'gulp-coffee'
mocha = require 'gulp-mocha'
groc = require 'gulp-groc'
clean = require 'gulp-clean'
cf = require 'coffee-script/register'

gulp.task 'develop', () ->
  gulp.watch('test/coffee/*.coffee', ['coffee'])

gulp.task 'cleandoc', () ->
  gulp.src('doc/*.html', {read: false})
    .pipe(clean())

gulp.task 'testdoc:availability', () ->
  gulp.src('availability.coffee', {cwd: './test'})
    .pipe(groc({ root: './test', out: './test/doc'}))

gulp.task 'testdoc:blackout', () ->
  gulp.src('blackout.coffee', {cwd: './test'})
    .pipe(groc({ root: './test', out: './test/doc'}))

gulp.task 'testdoc:bookable', () ->
  gulp.src('bookable.coffee', {cwd: './test'})
    .pipe(groc({ root: './test', out: './test/doc'}))

gulp.task 'testdoc:provider', () ->
  gulp.src('provider.coffee', {cwd: './test'})
    .pipe(groc({ root: './test', out: './test/doc'}))

gulp.task 'testdoc:reservation', () ->
  gulp.src('reservation.coffee', {cwd: './test'})
    .pipe(groc({ root: './test', out: './test/doc'}))

gulp.task 'testdoc:resource', () ->
  gulp.src('resource.coffee', {cwd: './test'})
    .pipe(groc({ root: './test', out: './test/doc'}))

gulp.task 'testdoc:requirementgroup', () ->
  gulp.src('requirementgroup.coffee', {cwd: './test'})
    .pipe(groc({ root: './test', out: './test/doc'}))

gulp.task 'testdoc:user', () ->
  gulp.src('user.coffee', {cwd: './test'})
    .pipe(groc({ root: './test', out: './test/doc'}))

gulp.task 'testdoc:whitelabel', () ->
  gulp.src('whitelabel.coffee', {cwd: './test'})
    .pipe(groc({ root: './test', out: './test/doc'}))

gulp.task 'testdoc:all', () ->
  gulp.src('*.coffee', {cwd: './test'})
    .pipe(groc({ root: './test', out: './test/doc'}))

gulp.task 'testdocs', ['cleandoc', 'testdoc:all']

gulp.task 'test:availability', () ->
  gulp.src('test/availability.coffee')
    .pipe(mocha())

gulp.task 'test:accessgroup', () ->
  gulp.src('test/accessgroup.coffee')
    .pipe(mocha())

gulp.task 'test:blackout', () ->
  gulp.src('test/blackout.coffee')
    .pipe(mocha())

gulp.task 'test:bookable', () ->
  gulp.src('test/bookable.coffee')
    .pipe(mocha())

gulp.task 'test:bookabletag', () ->
  gulp.src('test/bookabletag.coffee')
    .pipe(mocha())

gulp.task 'test:provider', () ->
  gulp.src('test/provider.coffee')
    .pipe(mocha())

gulp.task 'test:providergroup', () ->
  gulp.src('test/providergroup.coffee')
    .pipe(mocha())

gulp.task 'test:reservation', () ->
  gulp.src('test/reservation.coffee')
    .pipe(mocha())

gulp.task 'test:resource', () ->
  gulp.src('test/resource.coffee')
    .pipe(mocha())

gulp.task 'test:requirementgroup', () ->
  gulp.src('test/requirementgroup.coffee')
    .pipe(mocha())

gulp.task 'test:ticket', () ->
  gulp.src('test/ticket.coffee')
    .pipe(mocha())
    
gulp.task 'test:user', () ->
  gulp.src('test/user.coffee')
    .pipe(mocha())

gulp.task 'test:whitelabel', () ->
  gulp.src('test/whitelabel.coffee')
    .pipe(mocha())

gulp.task 'test', ['test:whitelabel', 'test:blackout', 'test:provider', 'test:providergroup', 'test:resource', 'test:requirementgroup', 'test:user', 'test:bookable', 'test:reservation', 'test:availability', 'test:bookabletag', 'test:ticket', 'test:accessgroup']
