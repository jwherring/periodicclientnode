# ## Blackout
#
# The tests in this file are written for the [mocha](http://visionmedia.github.io/mocha/) testing framework and use [should](https://github.com/visionmedia/should.js/) syntax as provided by the [Chai](http://chaijs.com/) assertion library
chai = require 'chai'
chaiaspromised = require 'chai-as-promised'
should = chai.should()
chai.use chaiaspromised
crypto = require 'crypto'
low = require 'lowdb'
ld = require 'lodash'

# Note that the test data filepath is relative to the project root dir.  It is important to run tests from the project root dir for this reason
db = low('test/db.json')
periodic = require '../periodic.coffee'
# Import the domain and (optionally) port number from `setup.coffee`
config = require './setup'
PeriodicGateway = periodic.PeriodicGateway
Provider = periodic.Provider
User = periodic.User
Blackout = periodic.Blackout

describe 'URL Instantiation Test', () ->
  
  endpoint = client = provider = user = blackout = {}

  before () ->
    # The endpoint is the domain name plus an optional port number.  Tests are run under the assumption that this domain points to a running version of the Periodic application
    endpoint = if config.port? then "#{config.domain}:#{config.port}" else "#{config.domain}"
    # ### Client Instantiation
    # Instantiate the client by passing your apikey and the endpoint of the running Periodic application to the `PeriodicGateway` class.  `PeriodicGateway` is a [singleton](http://en.wikipedia.org/wiki/Singleton_pattern), so this only needs to be done once
    client = PeriodicGateway.set(config.userkey, config.username, endpoint)
    # ### Resource Instantiation
    # URL schemas for particular resource are contained in classes named after the resource.
    provider = new Provider()
    provider_body = ld.clone db('providers').find({name: "Test Provider One"}).value()
    provider.create provider_body
      .then (res) ->
        provider = res
        blackout = new Blackout(provider.body.subdomain)

  after () ->
    provider.destroy()

  # #### URL Schema Test
  # This first test checks that the RESTful urls are correctly generated
  it 'should be able to create a blackout class and set up appropriate urls', () ->
    (blackout.posturl).should.equal("http://#{endpoint}/blackout")
    (blackout.indexurl).should.equal("http://#{endpoint}/blackout")

    # Resource locators for individual resources are only generated if the schema class is passed a resource id.  Note that this id must be a string.
    (blackout).should.not.have.property("idurl")

  it 'should set up an individual resource url if supplied with an id, which must be a string', () ->
    # Resource locators for individual resources are only generated if the schema class is passed both a provider subdomain and a resource id.  This id must be a string.  The provider subdomain exists to associate the resource with a provider, since all resource belong to providers.  It does not make sense to have a resource without a provider.
    blackout_one = new Blackout(provider.body.subdomain, '1')
    (blackout_one).should.have.property("idurl")
    (blackout_one.idurl).should.equal("http://#{endpoint}/blackout/id/1")

    blackout_two = new Blackout(provider.body.subdomain, 1)
    (blackout_two).should.not.have.property("idurl")

  it 'should fail to set up an individual resource url if not supplied with a provider subdomain, as Blackouts depend on individual providers', () ->
    # Since Blackouts depend on Providers, it is not possible to create a resource locator for an individual Blackout without saying which Provider it belongs to.  Passing in only a resource id, which works for standalone entities like providers and whitelabels, will not work for resources.
    blackout_one = new Blackout('1')
    blackout_one.should.not.have.property("idurl")

describe 'CREATE test', () ->

  endpoint = client = provider = user = blackout = {}

  # Create the endpoint, client, provider and user for all tests
  before () ->
    console.log "BEFORE"
    provider_body = ld.clone db('providers').find({name: "Test Provider One"}).value()
    endpoint = if config.port? then "#{config.domain}:#{config.port}" else "#{config.domain}"
    client = PeriodicGateway.set(config.userkey, config.username, endpoint)
    provider = new Provider()
    provider.create provider_body
      .then (res) ->
        provider = res
        console.log "CREATED PROVIDER #{provider.id}"

  # Clean up the created provider and user after the test has run
  after () ->
    providerid = provider.id
    console.log "AFTER"
    console.log provider
    provider.destroy()
      .then (res) ->
        console.log "DESTROYED PROVIDER #{providerid}"

  it 'should not be possible to create a blackout without a start', () ->

    # ### Fail Create Test
    # Test data is stored in a [lowdb](https://github.com/typicode/lowdb) database for convenience.  Try creating a blackout without the required start property and confirm that it fails
    test_blackout = ld.clone db('blackouts').find({name: "Test Blackout One"}).value()
    delete test_blackout.start
    bk = new Blackout(provider.body.subdomain)
    tr0 = bk.create test_blackout
    tr0.should.eventually.not.have.property "id"
    
  it 'should not be possible to create a blackout without an end', () ->

    # ### Fail Create Test
    # Test data is stored in a [lowdb](https://github.com/typicode/lowdb) database for convenience.  Try creating a blackout without the required start property and confirm that it fails
    test_blackout = ld.clone db('blackouts').find({name: "Test Blackout One"}).value()
    delete test_blackout.end
    bk = new Blackout(provider.body.subdomain)
    tr0 = bk.create test_blackout
    tr0.should.eventually.not.have.property "id"
    
  it 'should not be possible to create a blackout with an invalid start time', () ->

    # ### Fail Create Test
    # Test data is stored in a [lowdb](https://github.com/typicode/lowdb) database for convenience.  Try creating a blackout without the required start property and confirm that it fails
    test_blackout = ld.clone db('blackouts').find({name: "Test Blackout One"}).value()
    test_blackout.start = "tomorrow"
    bk = new Blackout(provider.body.subdomain)
    tr0 = bk.create test_blackout
    tr0.should.eventually.not.have.property "id"
    
  it 'should not be possible to create a blackout with an invalid end time', () ->

    # ### Fail Create Test
    # Test data is stored in a [lowdb](https://github.com/typicode/lowdb) database for convenience.  Try creating a blackout without the required start property and confirm that it fails
    test_blackout = ld.clone db('blackouts').find({name: "Test Blackout One"}).value()
    test_blackout.end = "next week"
    bk = new Blackout(provider.body.subdomain)
    tr0 = bk.create test_blackout
    tr0.should.eventually.not.have.property "id"
    
  it 'should not be possible to create a blackout with an end time that precedes the start time', () ->

    # ### Fail Create Test
    # Test data is stored in a [lowdb](https://github.com/typicode/lowdb) database for convenience.  Try creating a blackout without the required start property and confirm that it fails
    test_blackout = ld.clone db('blackouts').find({name: "Test Blackout One"}).value()
    st = test_blackout.start
    test_blackout.start = test_blackout.end
    test_blackout.end = st
    bk = new Blackout(provider.body.subdomain)
    tr0 = bk.create test_blackout
    tr0.should.eventually.not.have.property "id"

  it 'should be possible to create a blackout from a body object', () ->
    # ### Create Test
    # Test data is stored in a [lowdb](https://github.com/typicode/lowdb) database for convenience.  Retrieve a test resource, create it and confirm that it has been created and assigned default properties
    test_blackout = ld.clone db('blackouts').find({name: "Test Blackout One"}).value()
    rs = new Blackout(provider.body.subdomain)
    tr0 = rs.create test_blackout
      .then (bkb) ->
        blackout = bkb
    tr0.should.eventually.have.property "id"
    tr0.should.eventually.have.deep.property "body.start"
    tr0.should.eventually.have.deep.property "body.end"
    tr0.should.eventually.have.deep.property "body.name"

  it 'should be possible to retrieve the created blackout', () ->
    # ### Retrieve Test
    # Retrieve the blackout via the API to confirm that the call is working
    new_blackout = new Blackout(provider.body.subdomain, blackout.id)
    tr1 = new_blackout.retrieve()
    tr1.should.eventually.have.deep.property "body.start", blackout.body.start
    tr1.should.eventually.have.deep.property "body.end", blackout.body.end
    tr1.should.eventually.have.deep.property "body.name", blackout.body.name

  it 'should be possible to modify the created blackout', () ->
    # ### Update Test
    # Update the blackout via the API
    new_start = ld.clone db('misc').find({key: "start"}).value()
    new_start = new_start.value
    new_name = "Updated #{blackout.body.name}"
    nrs = blackout.update {name: new_name, start: new_start}
      .then (res) ->
        blackout.retrieve()
      .then (res) ->
        blackout = res
    nrs.should.eventually.have.deep.property "body.name", new_name
    nrs.should.eventually.have.deep.property "body.start", new_start
   
  it 'should not be possible to modify the created blackout with an invalid time', () ->
    # ### FAIL Update Test
    # Update the blackout via the API
    old_start = blackout.body.start
    nrs = blackout.update {start: "tomorrow"}
      .then (res) ->
        blackout.retrieve()
      .then (res) ->
        blackout = res
    nrs.should.eventually.have.deep.property "body.start", old_start
   
  it 'should not be possible to modify the created blackout with an invalid timespan', () ->
    # ### Update Test
    # Update the blackout via the API
    old_start = blackout.body.start
    old_end = blackout.body.end
    nrs = blackout.update {start: old_end, end: old_start}
      .then (res) ->
        blackout.retrieve()
      .then (res) ->
        blackout = res
    nrs.should.eventually.have.deep.property "body.start", old_start
    nrs.should.eventually.have.deep.property "body.end", old_end
   
  it 'should be possible to delete the created blackout', () ->
    # ### Delete Test 
    # Delete the blackout and then confirm that the list of blackouts for the provider is one less than before
    rs = blackout.destroy()
      .then (res) ->
        provider.retrieve()
    rs.should.eventually.have.deep.property "body.blackouts.length", 0
