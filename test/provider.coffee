# ## Provider
#
# The tests in this file are written for the [mocha](http://visionmedia.github.io/mocha/) testing framework and use [should](https://github.com/visionmedia/should.js/) syntax as provided by the [Chai](http://chaijs.com/) assertion library
chai = require 'chai'
chaiaspromised = require 'chai-as-promised'
should = chai.should()
chai.use chaiaspromised
crypto = require 'crypto'
low = require 'lowdb'
ld = require 'lodash'

# Note that the test data filepath is relative to the project root dir.  It is important to run tests from the project root dir for this reason
db = low('test/db.json')
periodic = require '../periodic.coffee'
# Import the domain and (optionally) port number from `setup.coffee`
config = require './setup'
PeriodicGateway = periodic.PeriodicGateway
Provider = periodic.Provider

describe 'URL Instantiation Test', () ->
  
  endpoint = client = provider = {}

  before () ->
    # The endpoint is the domain name plus an optional port number.  Tests are run under the assumption that this domain points to a running version of the Periodic application
    endpoint = if config.port? then "#{config.domain}:#{config.port}" else "#{config.domain}"
    # ### Client Instantiation
    # Instantiate the client by passing your apikey and the endpoint of the running Periodic application to the `PeriodicGateway` class.  `PeriodicGateway` is a [singleton](http://en.wikipedia.org/wiki/Singleton_pattern), so this only needs to be done once
    client = PeriodicGateway.set(config.userkey, config.username, endpoint)
    # ### Resource Instantiation
    # URL schemas for particular resource are contained in classes named after the resource.
    provider = new Provider()

  # #### URL Schema Test
  # This first test checks that the RESTful urls are correctly generated
  it 'should be able to create a provider class and set up appropriate urls', () ->
    (provider.posturl).should.equal("http://#{endpoint}/provider")
    (provider.indexurl).should.equal("http://#{endpoint}/provider")

    # Resource locators for individual resources are only generated if the schema class is passed a resource id.  Note that this id must be a string.
    (provider).should.not.have.property("idurl")

  it 'should set up an individual resource url if supplied with an id, which must be a string', () ->
    provider = new Provider('1')
    (provider).should.have.property("idurl")
    (provider.idurl).should.equal("http://#{endpoint}/provider/id/1")

    provider_two = new Provider(1)
    (provider_two).should.not.have.property("idurl")

describe 'CREATE Test', () ->
  
  # The client, endpoint and provider are reused for all tests.
  endpoint = client = provider = {}

  # Create the endpoint, client and provider for all tests
  before () ->
    endpoint = if config.port? then "#{config.domain}:#{config.port}" else "#{config.domain}"
    client = PeriodicGateway.set(config.userkey, config.username, endpoint)
    provider = new Provider()

  # Clean up the created provider after the test has run.
  after () ->
    resid = provider.id
    dest = provider.destroy()
    dest.then (res) ->
      console.log "DESTROYED PROVIDER #{resid}"

  it 'should not be able to create a provider with missing required fields', () ->

    # ### Fail Create Test
    # Test data is stored in a [lowdb](https://github.com/typicode/lowdb) database for convenience.  Get the first test provider ahead of making the CREATE call.  These are all the required fields.  Remove some fields to check that it fails
    test_provider_one = ld.clone db('providers').find({name: "Test Provider One"}).value()
    delete test_provider_one.name
    pv = new Provider()
    tp0 = pv.create test_provider_one
    tp0.should.eventually.not.have.property "id"
    

  it 'should be able to take a body object and create a corresponding provider', () ->

    # ### Create Test
    # Test data is stored in a [lowdb](https://github.com/typicode/lowdb) database for convenience.  Get the first test provider ahead of making the CREATE call.
    test_provider_one = db('providers').find({name: "Test Provider One"}).value()

    # CREATE a provider with this sample data
    tp1 = provider.create test_provider_one

    # Since network calls return a promise, use the [Chai-as-Promised](https://github.com/domenic/chai-as-promised/) `eventually` wrapper to check that the `idurl` and `body.id` data members have been set as a result.  Note that the global provider has to be reassigned in the resolution of the promise in order for it to be available to the next test.
    tp1.then (res) ->
      provider = res
    tp1.should.eventually.have.property "idurl"
    tp1.should.eventually.have.property "body"
    tp1.should.eventually.have.property "id"
    tp1.should.eventually.have.deep.property "body.id"

  it 'should be able to retrieve a newly created provider', () ->

    # ### Retrieve Test
    # Instantiate a new provider object with the id of the one we just created and try to populate it with data from the server.
    provider_two = new Provider(provider.id)
    tp2 = provider_two.retrieve()
    tp2.then (res) ->
      provider_two = res
    tp2.should.eventually.have.property "body"

    # Check that everything submitted was accepted without modification
    tp2.should.eventually.have.property "id", provider.id
    tp2.should.eventually.have.deep.property "body.email", provider.body.email
    tp2.should.eventually.have.deep.property "body.subdomain", provider.body.subdomain
    tp2.should.eventually.have.deep.property "body.firstname", provider.body.firstname
    tp2.should.eventually.have.deep.property "body.lastname", provider.body.lastname

    # Make sure that the defaults have been set by the add process
    #
    tp2.should.eventually.have.deep.property "body.regions"
    tp2.should.eventually.have.deep.property "body.blackouts"
    tp2.should.eventually.have.deep.property "body.resources"
    tp2.should.eventually.have.deep.property "body.reservationtypes"
    tp2.should.eventually.have.deep.property "body.users"

  it 'should not be able to create another provider with the same subdomain', () ->

    test_provider_two = ld.clone db('providers').find({name: "Test Provider One"}).value()
    pv2 = new Provider()
    tp5 = pv2.create test_provider_two
    tp5.should.eventually.not.have.property "id"

  it 'should be able to update a newly created provider', () ->

    # ### Update Test
    # Update the original provider with some sample data and verify that the information returned from the server is the same as the sample information passed in
    update_provider_one = db('providers').find({name: "Update Test Provider One"}).value()
    tp3 = provider.update update_provider_one
    tp3.should.eventually.have.deep.property "body.email", update_provider_one.email
    tp3.should.eventually.have.deep.property "body.name", update_provider_one.name
    tp3.should.eventually.have.deep.property "body.firstname", update_provider_one.firstname
    tp3.should.eventually.have.deep.property "body.lastname", update_provider_one.lastname
    tp3.should.eventually.have.deep.property "body.subdomain", update_provider_one.subdomain

  it 'should not be able to update a newly created provider with unauthorized properties', () ->
    #
    # ### Unauthorized Update Test
    # Attempt to update the original provider with some properties that are not allowed by the schema

    tp4 = provider.update {"notallowed": "property not allowed"}
    tp4.should.eventually.not.have.deep.property "body.notallowed"

