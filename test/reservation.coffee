# ## Reservation
#
# The tests in this file are written for the [mocha](http://visionmedia.github.io/mocha/) testing framework and use [should](https://github.com/visionmedia/should.js/) syntax as provided by the [Chai](http://chaijs.com/) assertion library
chai = require 'chai'
chaiaspromised = require 'chai-as-promised'
should = chai.should()
chai.use chaiaspromised
crypto = require 'crypto'
low = require 'lowdb'
ld = require 'lodash'

# Note that the test data filepath is relative to the project root dir.  It is important to run tests from the project root dir for this reason
db = low('test/db.json')
periodic = require '../periodic.coffee'
# Import the domain and (optionally) port number from `setup.coffee`
config = require './setup'
PeriodicGateway = periodic.PeriodicGateway
Bookable = periodic.Bookable
Provider = periodic.Provider
Reservation = periodic.Reservation
Resource = periodic.Resource
ResourceGroup = periodic.ResourceGroup

describe 'URL Instantiation Test', () ->
  
  endpoint = client = provider = reservation = {}

  before () ->
    # The endpoint is the domain name plus an optional port number.  Tests are run under the assumption that this domain points to a running version of the Periodic application
    endpoint = if config.port? then "#{config.domain}:#{config.port}" else "#{config.domain}"
    # ### Client Instantiation
    # Instantiate the client by passing your apikey and the endpoint of the running Periodic application to the `PeriodicGateway` class.  `PeriodicGateway` is a [singleton](http://en.wikipedia.org/wiki/Singleton_pattern), so this only needs to be done once
    client = PeriodicGateway.set(config.userkey, config.username, endpoint)
    # ### Resource Instantiation
    # URL schemas for particular resource are contained in classes named after the resource.
    provider = new Provider()
    provider_body = ld.clone db('providers').find({name: "Test Provider One"}).value()
    provider.create provider_body
      .then (res) ->
        provider = res
        reservation = new Reservation(provider.body.subdomain)

  after () ->
    provider.destroy()

  # #### URL Schema Test
  # This first test checks that the RESTful urls are correctly generated
  it 'should be able to create a provider class and set up appropriate urls', () ->
    (reservation.posturl).should.equal("http://#{endpoint}/reservation")
    (reservation.indexurl).should.equal("http://#{endpoint}/reservation")

    # Resource locators for individual resources are only generated if the schema class is passed a resource id.  Note that this id must be a string.
    (reservation).should.not.have.property("idurl")

  it 'should set up an individual reservation url if supplied with an id, which must be a string', () ->
    reservation = new Reservation(provider.body.subdomain, '1')
    (reservation).should.have.property("idurl")
    (reservation.idurl).should.equal("http://#{endpoint}/reservation/id/1")

    reservation_two = new Reservation(provider.body.subdomain, 1)
    (reservation_two).should.not.have.property("idurl")

describe 'CREATE Test', () ->
  # The client, endpoint and provider are reused for all tests.
  endpoint = client = provider = reservation = temp_reservation = resourceless_reservation = resource1 = resource2 = resource3 = resource4 = resource5 = resourcegroup = bookable1 = bookable2 = bookable3 = {}

  # Create the endpoint, client and provider for all tests
  before () ->
    endpoint = if config.port? then "#{config.domain}:#{config.port}" else "#{config.domain}"
    client = PeriodicGateway.set(config.userkey, config.username, endpoint)
    provider = new Provider()
    provider_body = ld.clone db('providers').find({name: "Test Provider One"}).value()
    resource1_body = ld.clone db('resources').find({name: "Test Resource One"}).value()
    resource2_body = ld.clone db('resources').find({name: "Test Resource Two"}).value()
    resource3_body = ld.clone db('resources').find({name: "Test Resource Three"}).value()
    resource4_body = ld.clone db('resources').find({name: "Test Resource Four"}).value()
    resource5_body = ld.clone db('resources').find({name: "Test Resource Five"}).value()
    bookable1_body = ld.clone db('bookables').find({name: "Test Bookable One"}).value()
    bookable2_body = ld.clone db('bookables').find({name: "Test Bookable Two"}).value()
    bookable3_body = ld.clone db('bookables').find({name: "Test Bookable Three"}).value()
    resourcegroup_body = ld.clone db('resourcegroups').find({name: "Test ResourceGroup One"}).value()
    provider.create provider_body
      .then (res) ->
        provider = res
        bookable1 = new Bookable(provider.body.subdomain)
        bookable1.create bookable1_body
      .then (res) ->
        bookable2 = new Bookable(provider.body.subdomain)
        bookable2.create bookable2_body
      .then (res) ->
        resource1 = new Resource(provider.body.subdomain)
        resource1.create resource1_body
      .then (res) ->
        resource2 = new Resource(provider.body.subdomain)
        resource2.create resource2_body
      .then (res) ->
        resource3 = new Resource(provider.body.subdomain)
        resource3.create resource3_body
      .then (res) ->
        resource4 = new Resource(provider.body.subdomain)
        resource4.create resource4_body
      .then (res) ->
        resource5 = new Resource(provider.body.subdomain)
        resource5.create resource5_body
      .then (res) ->
        bookable1.update {requiredresources: [resource1.id]}
      .then (res) ->
        resourcegroup_body.resources = [resource2.id, resource3.id]
        resourcegroup = new ResourceGroup(provider.body.subdomain)
        resourcegroup.create resourcegroup_body
      .then (res) ->
        bookable3_body.requirementgroups = [resourcegroup.body]
        bookable3 = new Bookable(provider.body.subdomain)
        bookable3.create bookable3_body
      .then (res) ->
        console.log "BOOKABLE THREE! #{res.id}"
        resource5 = res
        reservation = new Reservation(provider.body.subdomain)

  # Clean up the created provider after the test has run.
  after () ->
    resid = provider.id
    console.log "Don't forget to destroy provider #{resid}"
    #dest = provider.destroy()
    #dest.then (res) ->
    #  console.log "DESTROYED PROVIDER #{resid}"

  it.skip 'should not be able to create a reservation with missing required fields', () ->

    # ### Fail Create Test
    # Test data is stored in a [lowdb](https://github.com/typicode/lowdb) database for convenience.  Get the first test reservation ahead of making the CREATE call.  The fields given in the database are all the required fields minus the bookable - so it should not be possible to create a reservation without having added one of the precreated bookables
    test_reservation_one = ld.clone db('reservations').find({name: "Test Reservation One"}).value()
    delete test_reservation_one.name
    rs = new Reservation(provider.body.subdomain)
    trs0 = rs.create test_reservation_one
    trs0.should.eventually.not.have.property "id"

  it.skip 'should be able to take a body object and create a corresponding reservation once a bookable is added', () ->

    # ### Create Test
    # Test data is stored in a [lowdb](https://github.com/typicode/lowdb) database for convenience.  Get the first test reservation ahead of making the CREATE call.  Add a bookable to this information to meet the minimum requirements
    test_reservation_one = ld.clone db('reservations').find({name: "Test Reservation One"}).value()
    test_reservation_one.bookable = bookable1.id
    test_reservation_one.resources = [resource1.id]
    delete test_reservation_one.name #have to delete the name since it's not an allowed reservation property (but we use it for retrieval from test information db)
    rs = new Reservation(provider.body.subdomain)
    trs0 = rs.create test_reservation_one
      .then (res) ->
        reservation = res
    trs0.should.eventually.have.property "id"

  it.skip 'should be possible to retrieve the newly created reservation', () ->
    # ### Retrieve Test
    # Retrieve the newly created reservation via the API to confirm it exists (and that the expires property has been set)
    reservation_two = new Reservation(provider.body.subdomain, reservation.id)
    test_reservation_two = ld.clone db('reservations').find({name: "Test Reservation One"}).value()
    trs2 = reservation_two.retrieve()
    trs2.then (res) ->
      reservation_two = res
    trs2.should.eventually.have.deep.property "body.start", test_reservation_two.start
    trs2.should.eventually.have.deep.property "body.end", test_reservation_two.end
    trs2.should.eventually.have.deep.property "body.expires", test_reservation_two.end
    trs2.should.eventually.have.deep.property "body.bookable", bookable1.id

  it.skip 'should not be possible to book a second reservation at the same time due to resource conflict', () ->
    
    # ### FAIL Create Test
    # Since we have booked a reservation with resource1, it should not be possible to book another one at the same time.
    test_reservation_one = ld.clone db('reservations').find({name: "Test Reservation One"}).value()
    test_reservation_one.bookable = bookable1.id
    test_reservation_one.resources = [resource1.id]
    delete test_reservation_one.name #have to delete the name since it's not an allowed reservation property (but we use it for retrieval from test information db)
    rs = new Reservation(provider.body.subdomain)
    trs0 = rs.create test_reservation_one
    trs0.should.eventually.have.property "statuscode", 409
    trs0.should.eventually.not.have.property "id"

  it.skip 'should be possible to update the newly created reservation', () ->
    # ### Update Test
    # Pull a 
    update_info = ld.clone db('reservations').find({name: "Test Reservation Two"}).value()
    delete update_info.name
    update_info.resources = [resource1.id]
    trs3 = reservation.update update_info
    trs3.should.eventually.have.deep.property 'body.start', update_info.start
    trs3.should.eventually.have.deep.property 'body.end', update_info.end
    trs3.should.eventually.have.deep.property 'body.bookable', bookable1.id

  it.skip 'should be possible to cancel the newly created reservation', () ->
    # ### Update Test - Cancel
    # Using the cancel call, make sure the reservation shows cancelled upon return
    trs3 = reservation.cancel()
    trs3.should.eventually.have.deep.property 'body.status', "cancelled"

  it.skip 'should be possible to destroy the newly created reservation', () ->
    # ### Destroy Test
    # Instantiate a new reservation object with the id of the one we just created and try to populate it with data from the server.
    resid = reservation.id
    trs4 = reservation.destroy()
      .then (res) ->
        test_res = new Reservation(provider.body.subdomain, resid)
        test_res.retrieve()
      .then (res) ->
        test_res = res
    trs4.should.eventually.not.have.property "body.id"

  it.skip 'should be able to take a body object and create a corresponding reservation for a bookable with no required resources once a bookable is added', () ->

    # ### Create Test
    # Test data is stored in a [lowdb](https://github.com/typicode/lowdb) database for convenience.  Get the first test reservation ahead of making the CREATE call.  Add a bookable to this information to meet the minimum requirements
    test_reservation_three = ld.clone db('reservations').find({name: "Test Reservation One"}).value()
    test_reservation_three.bookable = bookable2.id
    delete test_reservation_three.name #have to delete the name since it's not an allowed reservation property (but we use it for retrieval from test information db)
    rs = new Reservation(provider.body.subdomain)
    trs10 = rs.create test_reservation_three
      .then (res) ->
        resourceless_reservation = res
    trs10.should.eventually.have.property "id"
    trs10.should.eventually.have.deep.property "body.status", "booked"

  it.skip 'should not be possible to book a second resourceless reservaton at the same time', () ->

    # ### Fail Create Test
    # Test data is stored in a [lowdb](https://github.com/typicode/lowdb) database for convenience.  Get the first test reservation ahead of making the CREATE call.  Add a bookable to this information to meet the minimum requirements
    test_reservation_four = ld.clone db('reservations').find({name: "Test Reservation One"}).value()
    test_reservation_four.bookable = bookable2.id
    delete test_reservation_four.name #have to delete the name since it's not an allowed reservation property (but we use it for retrieval from test information db)
    rs = new Reservation(provider.body.subdomain)
    trs11 = rs.create test_reservation_four
    trs11.should.eventually.have.property "statuscode", 409
    trs11.should.eventually.not.have.property "id"

  it.skip 'should be possible to create a temporary reservation', () ->
    # ### Create Test
    # Repeat the create test from before, but this time set a hold to confirm that the expires property is being set correctly - i.e. that it comes before the reservation end
    test_reservation_one = ld.clone db('reservations').find({name: "Test Reservation One"}).value()
    test_reservation_one.bookable = bookable1.id
    test_reservation_one.hold = 180
    test_reservation_one.resources = [resource1.id]
    delete test_reservation_one.name #have to delete the name since it's not an allowed reservation property (but we use it for retrieval from test information db)
    rs = new Reservation(provider.body.subdomain)
    trs0 = rs.create test_reservation_one
      .then (res) ->
        temp_reservation = res
    trs0.should.eventually.have.property "id"
    trs0.should.eventually.have.deep.property "body.expires"
    trs0.should.eventually.have.deep.property "body.hold", 180
    trs0.should.eventually.satisfy (trs) ->
      trs.body.expires.localeCompare(trs.body.end) < 0

  it.skip 'updating the hold on a temporary reservation should change the expires member', () ->
    # ### Update Test
    # The "expires" property should be updated as a sideeffect of renewing the hold on a reservation
    old_expires = temp_reservation.body.expires
    trs5 = temp_reservation.update {hold: 360}
      .then (res) ->
        temp_reservation = res
    trs5.should.eventually.have.deep.property "body.hold", 360
    trs5.should.eventually.satisfy (trs) ->
      trs.body.expires.localeCompare(old_expires) > 0

  it.skip 'should be possible to remove a hold on the temporary reservation, converting it to a permanent one', () ->
    trs6 = temp_reservation.removeHold()
      .then (res) ->
        temp_reservation = res
    trs6.should.eventually.not.have.deep.property "body.hold"
    trs6.should.eventually.satisfy (trs) ->
      trs.expires is trs.end

  it.skip 'should be possible to destroy the newly created reservation', () ->
    # ### Destroy Test
    # Clean up from previous test
    resid = temp_reservation.id
    trs4 = temp_reservation.destroy()
      .then (res) ->
        test_res = new Reservation(provider.body.subdomain, resid)
        test_res.retrieve()
      .then (res) ->
        test_res = res
    trs4.should.eventually.not.have.property "body.id"

  it 'should be possible to book an appointment on a bookable with a requirementgroup', () ->
    test_reservation_five = ld.clone db('reservations').find({name: "Test Reservation One"}).value()
    test_reservation_six = ld.clone db('reservations').find({name: "Test Reservation One"}).value()
    test_reservation_seven = ld.clone db('reservations').find({name: "Test Reservation One"}).value()
    delete test_reservation_five.name
    delete test_reservation_six.name
    delete test_reservation_seven.name
    test_reservation_five.bookable = bookable3.id
    test_reservation_five.resources = [resource2.id]
    test_reservation_six.bookable = bookable3.id
    test_reservation_six.resources = [resource3.id]
    test_reservation_seven.bookable = bookable3.id
    test_reservation_seven.resources = [resource2.id]
    trs5 = new Reservation(provider.body.subdomain)
    trs6 = new Reservation(provider.body.subdomain)
    trs7 = new Reservation(provider.body.subdomain)
    ts5 = trs5.create test_reservation_five
      .then (result) ->
        console.log "RESULT 5"
        console.log result
        trs6.create test_reservation_six
      .then (result) ->
        console.log "RESULT 6"
        console.log result
        trs7.create test_reservation_seven
      .then (result) ->
        console.log "RESULT 7"
        console.log result
        result
    ts5.should.eventually.not.have.property 'id'
