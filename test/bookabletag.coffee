# ## Bookable Tag
#
# The tests in this file are written for the [mocha](http://visionmedia.github.io/mocha/) testing framework and use [should](https://github.com/visionmedia/should.js/) syntax as provided by the [Chai](http://chaijs.com/) assertion library
#
chai = require 'chai'
chaiaspromised = require 'chai-as-promised'
should = chai.should()
chai.use chaiaspromised
crypto = require 'crypto'
low = require 'lowdb'
ld = require 'lodash'

# Note that the test data filepath is relative to the project root dir.  It is important to run tests from the project root dir for this reason
db = low('test/db.json')
periodic = require '../periodic.coffee'
# Import the domain and (optionally) port number from `setup.coffee`
config = require './setup'
PeriodicGateway = periodic.PeriodicGateway
Provider = periodic.Provider
Bookable = periodic.Bookable
Resource = periodic.Resource
BookableTag = periodic.BookableTag

describe 'URL Instantiation Test', () ->
  
  endpoint = client = provider = bookabletag = {}

  before () ->
    # The endpoint is the domain name plus an optional port number.  Tests are run under the assumption that this domain points to a running version of the Periodic application
    endpoint = if config.port? then "#{config.domain}:#{config.port}" else "#{config.domain}"
    # ### Client Instantiation
    # Instantiate the client by passing your apikey and the endpoint of the running Periodic application to the `PeriodicGateway` class.  `PeriodicGateway` is a [singleton](http://en.wikipedia.org/wiki/Singleton_pattern), so this only needs to be done once
    client = PeriodicGateway.set(config.userkey, config.username, endpoint)
    # ### Resource Instantiation
    # URL schemas for particular resources are contained in classes named after the resource.
    provider = new Provider()
    provider_body = ld.clone db('providers').find({name: "Test Provider One"}).value()
    provider.create provider_body
      .then (res) ->
        bookabletag = new BookableTag()
        res

  after () ->
    provider.destroy()

  # #### URL Schema Test
  # This first test checks that the RESTful urls are correctly generated
  it 'should be able to create a resource class and set up appropriate urls', () ->
    (bookabletag.posturl).should.equal("http://#{endpoint}/tag")
    (bookabletag.indexurl).should.equal("http://#{endpoint}/tag")

    # Resource locators for individual resources are only generated if the schema class is passed a resource id.  Note that this id must be a string.
    (bookabletag).should.not.have.property("idurl")

describe 'CREATE test', () ->

  endpoint = client = provider1 = provider2 = bookable1 = bookable2 = bookabletag = {}
  taglength = 0

  before () ->
    client = PeriodicGateway.set(config.userkey, config.username, endpoint)
    # ### Resource Instantiation
    # URL schemas for particular resources are contained in classes named after the resource.
    provider1 = new Provider()
    provider2 = new Provider()
    provider1_body = ld.clone db('providers').find({name: "Test Provider One"}).value()
    provider2_body = ld.clone db('providers').find({name: "Test Provider Two"}).value()
    bookable1_body = ld.clone db('bookables').find({name: "Test Bookable One"}).value()
    bookable2_body = ld.clone db('bookables').find({name: "Test Bookable Two"}).value()
    provider1.create provider1_body
      .then (res) ->
        provider2.create provider2_body
      .then (res) ->
        bookable1 = new Bookable(provider1.body.subdomain)
        bookable1.create bookable1_body
      .then (res) ->
        console.log "BOOKABLE1"
        console.log bookable1
        bookable2 = new Bookable(provider2.body.subdomain)
        bookable2.create bookable2_body
      .then (res) ->
        bookabletag = new BookableTag()
        res

  after () ->
    provider1.destroy()
      .then (res) ->
        provider2.destroy()

  it 'should be possible to get a list of tags available on a whitelabel', () ->

    # ### INDEX Test
    # Calling 'index' on a BookableTag object will return the list of tags that have been set on any bookable for that whitelabel.  
    bookabletag = new BookableTag()
    bt = bookabletag.index()
      .then (res) ->
        console.log "TAGS:"
        console.log res
        res
    bt.should.eventually.have.property 'statuscode', 200
    bt.should.eventually.have.property 'body'
    bt.should.eventually.have.deep.property 'body.length'
    bt.should.eventually.satisfy (rs) ->
      taglength = rs.body.length
      console.log taglength
      Object.prototype.toString.call(rs.body) is '[object Array]'

  it 'should be possible to add a new tag and confirm that the number of tags has increased by one', () ->

    # ### INDEX Test
    # After adding a brand new tag to a bookable, we should get a list of tags that's one longer than the list we got before
    tg = new BookableTag()
    nb1 = new Bookable(provider1.body.subdomain, bookable1.id)
    newtag = nb1.retrieve()
      .then (res) ->
        nb1.update {tags: ['thistagneverexisted']}
      .then (res) ->
        tg.index()
    newtag.should.eventually.have.property 'statuscode', 200
    newtag.should.eventually.have.property 'body'
    newtag.should.eventually.have.deep.property 'body.length'
    newtag.should.eventually.satisfy (rs) ->
      Object.prototype.toString.call(rs.body) is '[object Array]'
    newtag.should.eventually.satisfy (rs) ->
      rs.body.length = taglength + 1

  it 'should be possible to get a list of bookables by passing in a tag', () ->

    # ### SEARCH Test
    # Since the point of tags is to get groups of bookables, it should be possible to get a list of bookables by passing in a tag to the params of a 'search' call
    tg = new BookableTag()
    params =
      tags: ['thistagneverexisted']
    bookables = tg.search params
    bookables.should.eventually.have.property 'statuscode', 200
    bookables.should.eventually.have.property 'body'
    bookables.should.eventually.have.deep.property 'body.length'
    bookables.should.eventually.satisfy (rs) ->
      Object.prototype.toString.call(rs.body) is '[object Array]'
    bookables.should.eventually.satisfy (rs) ->
      ld.every rs.body, (bkb) ->
        bkb.type is 'bookable'
    bookables.should.eventually.satisfy (rs) ->
      rs.body.length is 1
    bookables.should.eventually.satisfy (rs) ->
      rs.body[0].id is bookable1.id

  it 'should be possible to get multiple bookables back from different providers by passing in a list of tags', () ->

    # ### SEARCH Test
    # Repeat the previous test, but now with an additional tag on a bookable on a different provider.  Pass in both tags as a string with the names of the tags separated by commas, and the system should return both bookables.
    tg = new BookableTag()
    params =
      tags: 'thistagneverexisted,thisoneneverexistedeither'
    nb2 = new Bookable(provider2.body.subdomain, bookable2.id)
    bookables = nb2.retrieve()
      .then (res) ->
        nb2.update {tags: ['thisoneneverexistedeither']}
      .then (res) ->
        tg.search params
    bookables.should.eventually.have.property 'statuscode', 200
    bookables.should.eventually.have.property 'body'
    bookables.should.eventually.have.deep.property 'body.length'
    bookables.should.eventually.satisfy (rs) ->
      Object.prototype.toString.call(rs.body) is '[object Array]'
    bookables.should.eventually.satisfy (rs) ->
      ld.every rs.body, (bkb) ->
        bkb.type is 'bookable'
    bookables.should.eventually.satisfy (rs) ->
      console.log "RESULT"
      console.log rs.body
      rs.body.length is 2

  it 'should be possible to delete a tag from all bookables that have it', () ->

    # ### DELETE Test
    # The API provides an interface for deleting a particular tag from all bookables on which it exists.  It works in the normal way, but with the name of the tag passed in as the 'id' in the URL.  Confirm that after deleting the tag created in the previous test we're back down to just one tag.
    tg = new BookableTag('thisoneneverexistedeither')
    minusone = tg.destroy()
      .then (res) ->
        console.log "DELETE TAG"
        console.log res
        tg.index()
      .then (res) ->
        console.log "INDEX AFTER DELETE TAG"
        console.log res
        res
    minusone.should.eventually.have.property 'statuscode', 200
    minusone.should.eventually.have.property 'body'
    minusone.should.eventually.have.deep.property 'body.length'
    minusone.should.eventually.satisfy (rs) ->
      Object.prototype.toString.call(rs.body) is '[object Array]'
    minusone.should.eventually.satisfy (rs) ->
      rs.body.length is 1
