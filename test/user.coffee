# ## User
#
# The tests in this file are written for the [mocha](http://visionmedia.github.io/mocha/) testing framework and use [should](https://github.com/visionmedia/should.js/) syntax as provided by the [Chai](http://chaijs.com/) assertion library
chai = require 'chai'
chaiaspromised = require 'chai-as-promised'
should = chai.should()
chai.use chaiaspromised
crypto = require 'crypto'
low = require 'lowdb'
ld = require 'lodash'

# Note that the test data filepath is relative to the project root dir.  It is important to run tests from the project root dir for this reason
db = low('test/db.json')
periodic = require '../periodic.coffee'
# Import the domain and (optionally) port number from `setup.coffee`
config = require './setup'
PeriodicGateway = periodic.PeriodicGateway
Provider = periodic.Provider
User = periodic.User

describe 'URL Instantiation Test', () ->
  
  endpoint = client = provider = user = {}

  before () ->
    # The endpoint is the domain name plus an optional port number.  Tests are run under the assumption that this domain points to a running version of the Periodic application
    endpoint = if config.port? then "#{config.domain}:#{config.port}" else "#{config.domain}"
    # ### Client Instantiation
    # Instantiate the client by passing your apikey and the endpoint of the running Periodic application to the `PeriodicGateway` class.  `PeriodicGateway` is a [singleton](http://en.wikipedia.org/wiki/Singleton_pattern), so this only needs to be done once
    client = PeriodicGateway.set(config.userkey, config.username, endpoint)
    # ### Resource Instantiation
    # URL schemas for particular resource are contained in classes named after the resource.
    user = new User()

  # #### URL Schema Test
  # This first test checks that the RESTful urls are correctly generated
  it 'should be able to create a user class and set up appropriate urls', () ->
    (user.posturl).should.equal("http://#{endpoint}/user")
    (user.indexurl).should.equal("http://#{endpoint}/user")

    # Resource locators for individual resources are only generated if the schema class is passed a resource id.  Note that this id must be a string.
    (user).should.not.have.property("idurl")

  it 'should set up an individual resource url if supplied with an id, which must be a string', () ->
    user_one = new User('1')
    (user_one).should.have.property("idurl")
    (user_one.idurl).should.equal("http://#{endpoint}/user/id/1")

    user_two = new User(1)
    (user_two).should.not.have.property("idurl")

describe 'CREATE test', () ->

  endpoint = client = provider = user = user2 = {}

  # Create the endpoint, client, provider and user for all tests
  before () ->
    console.log "BEFORE"
    provider_body = ld.clone db('providers').find({name: "Test Provider One"}).value()
    endpoint = if config.port? then "#{config.domain}:#{config.port}" else "#{config.domain}"
    client = PeriodicGateway.set(config.userkey, config.username, endpoint)
    provider = new Provider()
    user = new User()
    provider.create provider_body
      .then (res) ->
        provider = res
        console.log "CREATED PROVIDER"
        console.log provider.id

  # Clean up the created provider and user after the test has run
  after () ->
    providerid = provider.id
    console.log "AFTER"
    console.log provider
    userid = user.id
    dest = provider.destroy()
    dest.then (res) ->
      console.log "DESTROYED PROVIDER #{providerid}"
      console.log "DESTROYED USER #{userid}"

  it 'should not be possible to create a user with missing required fields', () ->

    # ### Fail Create Test
    # Test data is stored in a [lowdb](https://github.com/typicode/lowdb) database for convenience.  Get the first test provider ahead of making the CREATE call.  These are all the required fields.  Remove some fields to check that it fails
    test_user = ld.clone db('users').find({lastname: "One"}).value()
    delete test_user.password
    us = new User()
    tu0 = us.create test_user
    tu0.should.eventually.not.have.property "id"
    
    test_user = ld.clone db('users').find({lastname: "One"}).value()
    delete test_user.firstname
    us = new User()
    tu0 = us.create test_user
    tu0.should.eventually.not.have.property "id"

    test_user = ld.clone db('users').find({lastname: "One"}).value()
    delete test_user.lastname
    us = new User()
    tu0 = us.create test_user
    tu0.should.eventually.not.have.property "id"

  it 'should not be possible to create a user with a higher permission level', () ->
    # ### Fail Create Test
    # Test data is stored in a [lowdb](https://github.com/typicode/lowdb) database for convenience.  Get the first test user ahead of making the CREATE call.
    # Try to set the permission higher than our user's own.  This should fail.
    test_user = ld.clone db('users').find({lastname: "One"}).value()
    test_user.role = "admin"
    us = new User()
    tu1 = us.create test_user
    tu1.should.eventually.not.have.property "id"

  it 'should not be possible to create a user with provider- or employee-level permission without specifying a provider', () ->
    # ### Fail Create Test
    # Test data is stored in a [lowdb](https://github.com/typicode/lowdb) database for convenience.  Get the first test user ahead of making the CREATE call.
    # Periodic determines the provider through the subdomain on the endpoint URL: e.g. http://provider.marketplace.tld/entity.  Since we haven't set this on 
    # the client yet, attempts to create users with permission levels that require a provider (i.e. 'provider' or 'employee') should fail.
    test_user = ld.clone db('users').find({lastname: "One"}).value()
    us = new User()
    tu1 = us.create test_user
    tu1.should.eventually.not.have.property "id"

  it 'should be able to take a body object and create a corresponding user', () ->
    # ### Create Test
    # Test data is stored in a [lowdb](https://github.com/typicode/lowdb) database for convenience.  Get the first test user ahead of making the CREATE call.
    # In order for the call to work, we have to tell Periodic which provider to associated with the user (since we're trying to create and employee-level user, which 
    # requires a provider).  So, give the User object a 'provider' member set to the subdomain of the provider that was created in the setup to the tests (i.e. in the "before" method call).
    test_user = ld.clone db('users').find({lastname: "One"}).value()
    tu1_slug = [test_user.firstname.toLowerCase(), test_user.lastname.toLowerCase()].join('_')
    user = new User()
    user.provider = provider.body.subdomain
    tu1 = user.create test_user
      .then (usr) ->
        user = usr
    tu1.should.eventually.have.property "id"
    tu1.should.eventually.have.property "body"
    tu1.should.eventually.have.deep.property "body.role", "employee"
    tu1.should.eventually.have.deep.property "body.slug", tu1_slug

  it 'should not be possible to create another user with the same username', () ->
    # ### Fail Create Test (Slug)
    # Confirm that we cannot create a second user with the same username (slug); slugs should be unique
    # among users for a given whitelabel.
    test_user_two = ld.clone db('users').find({lastname: "One"}).value()
    user_two = new User()
    tu2 = user_two.create test_user_two
    tu2.should.eventually.not.have.property "id"

  it 'should be possible to retrieve the newly created user', () ->
    # ### Retrieve User
    # Create a new client user object, give it the id of the user just created, retrieve the user 
    # with this id from the remote application and confirm that it has the attributes assigned to the 
    # user previously created.
    new_test_user = ld.clone db('users').find({lastname: "One"}).value()
    new_user = new User(user.id)
    nu = new_user.retrieve()
    nu.should.eventually.have.deep.property "body.id"
    nu.should.eventually.have.deep.property "body.firstname", new_test_user.firstname
    nu.should.eventually.have.deep.property "body.lastname", new_test_user.lastname

  it 'should be possible to modify fields on the user', () ->
    # ### Update Test
    # Update the original user with some sample data and verify that the information returned from the server is the same as the sample information passed in
    test_user_two = ld.clone db('users').find({lastname: "Two"}).value()
    #tu2 = user.update({lastname: test_user_two.lastname})
    tu2 = user.update test_user_two
      .then (usr) ->
        user = usr
    tu2.should.eventually.have.deep.property "body.lastname", test_user_two.lastname
    tu2.should.eventually.have.deep.property "body.slug", test_user_two.slug
    tu2.should.eventually.have.deep.property "body.role", test_user_two.role

  it 'should not be possible to change the permission level to one higher than the user issuing the request', () ->
    # ### Fail Update Test (Permssion)
    # Try to give the user a permission higher than the API user making the request is allowed to give.  This should fail.
    console.log "ROLE TEST"
    test_user_two = ld.clone db('users').find({lastname: "Two"}).value()
    tu2 = user.update({role: "admin"})
    tu2.should.eventually.have.deep.property "body.role", test_user_two.role

  it 'should not be possible to change the username on a user to one that already exists', () ->
    # ### Fail Update Test (Slug)
    # Create another user and try to give it the same username (slug) as the already-created user.  This should fail, as the application forces slugs to be unique for a given whitelabel.
    test_user_one = ld.clone db('users').find({lastname: "One"}).value()
    tu1_slug = [test_user_one.firstname.toLowerCase(), test_user_one.lastname.toLowerCase()].join('_')
    user2 = new User()
    user2.provider = provider.body.subdomain
    tu3 = user2.create test_user_one
      .then (usr) ->
        console.log "SLUG TEST: "
        console.log usr
        usr.update {slug: user.slug}
    tu3.should.eventually.have.deep.property "body.slug", tu1_slug
        
  it 'should be possible to delete a user', () ->
    # ### Delete Test
    # Delete the user created in the previous test and confirm that it can no longer be retrieved.
    # Note that this tests for an id property on the body to confirm that retrieval failed.  Since the 
    # id property was set explicitly, the object has an id property regardless of whether the corresponding 
    # object exists in the remote database.
    u2id = user2.id
    u3 = new User(user2.id)
    u2dest = user2.destroy()
      .then (usr) ->
        u3.retrieve()
    u2dest.should.eventually.not.have.deep.property "body.id"
