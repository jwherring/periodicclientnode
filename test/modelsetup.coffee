# ##Config File
# The tests use a file called `setup.coffee`, which should mirror this one.  
# Change the values in the `exports` object to values appropriate for your setup to run the
# tests against your own endpoint.  (Note: the port should not be included
# if you are testing against the live API.  If that is the case, simply delete
# the "port" field from the `exports` object.)

module.exports =
  domain: "your_domain_endpoint_here (e.g. periodic.is)"
  port: "9001"
  userkey: "your_api_key_here"
  username: "your_username_here"


