# ## Resource Group
#
# The tests in this file are written for the [mocha](http://visionmedia.github.io/mocha/) testing framework and use [should](https://github.com/visionmedia/should.js/) syntax as provided by the [Chai](http://chaijs.com/) assertion library
chai = require 'chai'
chaiaspromised = require 'chai-as-promised'
should = chai.should()
chai.use chaiaspromised
crypto = require 'crypto'
low = require 'lowdb'
ld = require 'lodash'

# Note that the test data filepath is relative to the project root dir.  It is important to run tests from the project root dir for this reason
db = low('test/db.json')
periodic = require '../periodic.coffee'
# Import the domain and (optionally) port number from `setup.coffee`
config = require './setup'
PeriodicGateway = periodic.PeriodicGateway
Provider = periodic.Provider
Resource = periodic.Resource
ResourceGroup = periodic.ResourceGroup

describe 'URL Instantiation Test', () ->
  
  endpoint = client = provider = user = resource1 = resource2 = resourcegroup = {}

  before () ->
    # The endpoint is the domain name plus an optional port number.  Tests are run under the assumption that this domain points to a running version of the Periodic application
    endpoint = if config.port? then "#{config.domain}:#{config.port}" else "#{config.domain}"
    # ### Client Instantiation
    # Instantiate the client by passing your apikey and the endpoint of the running Periodic application to the `PeriodicGateway` class.  `PeriodicGateway` is a [singleton](http://en.wikipedia.org/wiki/Singleton_pattern), so this only needs to be done once
    client = PeriodicGateway.set(config.userkey, config.username, endpoint)
    # ### Resource Instantiation
    # URL schemas for particular resource are contained in classes named after the resource.
    provider = new Provider()
    provider_body = ld.clone db('providers').find({name: "Test Provider One"}).value()
    provider.create provider_body
      .then (res) ->
        provider = res
        resourcegroup = new ResourceGroup(provider.body.subdomain)

  after () ->
    provider.destroy()

  # #### URL Schema Test
  # This first test checks that the RESTful urls are correctly generated
  it 'should be able to create a resource class and set up appropriate urls', () ->
    (resourcegroup.posturl).should.equal("http://#{endpoint}/requirementgroup")
    (resourcegroup.indexurl).should.equal("http://#{endpoint}/requirementgroup")

    # Resource locators for individual resources are only generated if the schema class is passed a resource id.  Note that this id must be a string.
    (resourcegroup).should.not.have.property("idurl")

  it 'should set up an individual resource url if supplied with an id, which must be a string', () ->
    # Resource locators for individual resourcegroupss are only generated if the schema class is passed both a provider subdomain and a resource id.  This id must be a string.  The provider subdomain exists to associate the resourcegroup with a provider, since all resourcegroups belong to providers.  It does not make sense to have a resourcegroup without a provider.
    resourcegroup_one = new ResourceGroup(provider.body.subdomain, '1')
    (resourcegroup_one).should.have.property("idurl")
    (resourcegroup_one.idurl).should.equal("http://#{endpoint}/requirementgroup/id/1")

    resourcegroup_two = new ResourceGroup(provider.body.subdomain, 1)
    (resourcegroup_two).should.not.have.property("idurl")

  it 'should fail to set up an individual resource url if not supplied with a provider subdomain, as ResourceGroups depend on individual providers', () ->
    # Since resourcegroups depend on providers, it is not possible to create a resource locator for an individual resource without saying which provider it belongs to.  Passing in only a resource id, which works for standalone entities like providers and whitelabels, will not work for resourcegroups.
    resource_one = new ResourceGroup('1')
    resource_one.should.not.have.property("idurl")

describe 'CREATE test', () ->

  endpoint = client = provider = user = resource1 = resource2 = resourcegroup = {}

  # Create the endpoint, client, provider and user for all tests
  before () ->
    console.log "BEFORE"
    provider_body = ld.clone db('providers').find({name: "Test Provider One"}).value()
    resource1_body = ld.clone db('resources').find({name: "Test Resource One"}).value()
    resource2_body = ld.clone db('resources').find({name: "Test Resource Two"}).value()
    endpoint = if config.port? then "#{config.domain}:#{config.port}" else "#{config.domain}"
    client = PeriodicGateway.set(config.userkey, config.username, endpoint)
    provider = new Provider()
    provider.create provider_body
      .then (res) ->
        console.log "CREATED PROVIDER #{res.id}"
        provider = res
      .then (res) ->
        resource1 = new Resource(provider.body.subdomain)
        resource1.create(resource1_body)
      .then (res) ->
        console.log "CREATED RESOURCE #{res.id}"
        resource1 = res
        resource2 = new Resource(provider.body.subdomain)
        resource2.create(resource2_body)
      .then (res) ->
        console.log "CREATED RESOURCE #{res.id}"
        resource2 = res

  # Clean up the created provider and user after the test has run
  after () ->
    providerid = provider.id
    resource1id = resource1.id
    resource2id = resource2.id
    console.log "AFTER"
    console.log provider
    provider.destroy()
      .then (res) ->
        console.log "DESTROYED PROVIDER #{providerid} and associated RESOURCEs"

  it 'should not be possible to create a resourcegroup without a name', () ->

    # ### Fail Create Test
    # Test data is stored in a [lowdb](https://github.com/typicode/lowdb) database for convenience.  Try creating a resourcegroup without the required name property and confirm that it fails
    test_resourcegroup = ld.clone db('resourcegroups').find({name: "Test ResourceGroup One"}).value()
    delete test_resourcegroup.name
    console.log test_resourcegroup
    bk = new ResourceGroup(provider.body.subdomain)
    tr0 = bk.create test_resourcegroup
      .then (res) ->
        resourcegroup = res
    tr0.should.eventually.not.have.property "id"
    
  it 'should not be possible to create a resourcegroup from a body object with nonexistent resources', () ->
    # ### Fail Create Test
    # Test data is stored in a [lowdb](https://github.com/typicode/lowdb) database for convenience.  Try creating a resourcegroup with fake resource ids and confirm that it fails
    test_resourcegroup = ld.clone db('resourcegroups').find({name: "Test ResourceGroup One"}).value()
    test_resourcegroup.resources = ['nonexistent']
    bk = new ResourceGroup(provider.body.subdomain)
    tr0 = bk.create test_resourcegroup
      .then (res) ->
        resourcegroup = res
    tr0.should.eventually.not.have.property "id"

  it 'should be possible to create a resourcegroup from a body object, even without resources', () ->
    # ### Create Test
    # Test data is stored in a [lowdb](https://github.com/typicode/lowdb) database for convenience.  Retrieve a test resource, create it and confirm that it has been created and assigned default properties
    test_resourcegroup = ld.clone db('resourcegroups').find({name: "Test ResourceGroup One"}).value()
    test_resourcegroup.resources = []
    console.log "TEST_RESOURCEGROUP"
    console.log test_resourcegroup
    rs = new ResourceGroup(provider.body.subdomain)
    tr0 = rs.create test_resourcegroup
      .then (bkb) ->
        console.log "CREATED RESOURCEGROUP"
        console.log bkb
        resourcegroup = bkb
    tr0.should.eventually.have.property "id"
    tr0.should.eventually.have.deep.property "body.name"
    tr0.should.eventually.have.deep.property "body.resources.length", 0

  it 'should be possible to retrieve the created resourcegroup', () ->
    # ### Retrieve Test
    # Retrieve the resourcegroup via the API to confirm that the call is working
    new_resourcegroup = new ResourceGroup(provider.body.subdomain, resourcegroup.id)
    tr1 = new_resourcegroup.retrieve()
    tr1.should.eventually.have.deep.property "body.resources.length", resourcegroup.body.resources.length
    tr1.should.eventually.have.deep.property "body.name", resourcegroup.body.name

  it 'should not be possible to modify the created resourcegroup with a ficitional resource', () ->
    # ### FAIL Update Test
    # Confirm that we cannot add fictional resources to the resourcegroup after creation
    old_length = resourcegroup.body.resources.length
    new_resources = ld.clone resourcegroup.body.resources
    new_resources.push 'nonexistent'
    nrs = resourcegroup.update {resources: new_resources}
    nrs.should.eventually.have.deep.property "body.resources.length", old_length
    
  it 'should be possible to add existing resources to the resourcegroup', () ->
    # ### Update Test
    # Confirm that we can add existing resources to the resourcegroup after creation
    new_resources = ld.clone resourcegroup.body.resources
    new_resources.push resource1.id
    new_resources.push resource2.id
    new_length = new_resources.length
    nrs = resourcegroup.update {resources: new_resources}
      .then (res) ->
        console.log "RETRIEVE AFTER UPDATE"
        console.log res
        resourcegroup = res
    nrs.should.eventually.have.deep.property "body.resources.length", new_length

  it 'should be the case that if a resource is deleted, its removed from the requirementgroup', () ->
    old_length = resourcegroup.body.resources.length
    drs = resource1.destroy()
      .then (destroyed) ->
        resourcegroup.retrieve()
    drs.should.eventually.have.deep.property "body.resources.length", old_length - 1
