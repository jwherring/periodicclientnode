# ## Resource
#
# The tests in this file are written for the [mocha](http://visionmedia.github.io/mocha/) testing framework and use [should](https://github.com/visionmedia/should.js/) syntax as provided by the [Chai](http://chaijs.com/) assertion library
chai = require 'chai'
chaiaspromised = require 'chai-as-promised'
should = chai.should()
chai.use chaiaspromised
crypto = require 'crypto'
low = require 'lowdb'
ld = require 'lodash'

# Note that the test data filepath is relative to the project root dir.  It is important to run tests from the project root dir for this reason
db = low('test/db.json')
periodic = require '../periodic.coffee'
# Import the domain and (optionally) port number from `setup.coffee`
config = require './setup'
PeriodicGateway = periodic.PeriodicGateway
Provider = periodic.Provider
User = periodic.User
Resource = periodic.Resource

describe 'URL Instantiation Test', () ->
  
  endpoint = client = provider = user = resource = {}

  before () ->
    # The endpoint is the domain name plus an optional port number.  Tests are run under the assumption that this domain points to a running version of the Periodic application
    endpoint = if config.port? then "#{config.domain}:#{config.port}" else "#{config.domain}"
    # ### Client Instantiation
    # Instantiate the client by passing your apikey and the endpoint of the running Periodic application to the `PeriodicGateway` class.  `PeriodicGateway` is a [singleton](http://en.wikipedia.org/wiki/Singleton_pattern), so this only needs to be done once
    client = PeriodicGateway.set(config.userkey, config.username, endpoint)
    # ### Resource Instantiation
    # URL schemas for particular resource are contained in classes named after the resource.
    provider = new Provider()
    provider_body = ld.clone db('providers').find({name: "Test Provider One"}).value()
    provider.create provider_body
      .then (res) ->
        provider = res
        resource = new Resource(provider.body.subdomain)

  after () ->
    provider.destroy()

  # #### URL Schema Test
  # This first test checks that the RESTful urls are correctly generated
  it 'should be able to create a resource class and set up appropriate urls', () ->
    (resource.posturl).should.equal("http://#{endpoint}/resource")
    (resource.indexurl).should.equal("http://#{endpoint}/resource")

    # Resource locators for individual resources are only generated if the schema class is passed a resource id.  Note that this id must be a string.
    (resource).should.not.have.property("idurl")

  it 'should set up an individual resource url if supplied with an id, which must be a string', () ->
    # Resource locators for individual resources are only generated if the schema class is passed both a provider subdomain and a resource id.  This id must be a string.  The provider subdomain exists to associate the resource with a provider, since all resource belong to providers.  It does not make sense to have a resource without a provider.
    resource_one = new Resource(provider.body.subdomain, '1')
    (resource_one).should.have.property("idurl")
    (resource_one.idurl).should.equal("http://#{endpoint}/resource/id/1")

    resource_two = new Resource(provider.body.subdomain, 1)
    (resource_two).should.not.have.property("idurl")

  it 'should fail to set up an individual resource url if not supplied with a provider subdomain, as Resources depend on individual providers', () ->
    # Since resources depend on providers, it is not possible to create a resource locator for an individual resource without saying which provider it belongs to.  Passing in only a resource id, which works for standalone entities like providers and whitelabels, will not work for resources.
    resource_one = new Resource('1')
    resource_one.should.not.have.property("idurl")

describe 'CREATE test', () ->

  endpoint = client = provider = user = resource = {}

  # Create the endpoint, client, provider and user for all tests
  before () ->
    console.log "BEFORE"
    provider_body = ld.clone db('providers').find({name: "Test Provider One"}).value()
    endpoint = if config.port? then "#{config.domain}:#{config.port}" else "#{config.domain}"
    client = PeriodicGateway.set(config.userkey, config.username, endpoint)
    provider = new Provider()
    provider.create provider_body
      .then (res) ->
        provider = res
        console.log "CREATED PROVIDER #{provider.id}"

  # Clean up the created provider and user after the test has run
  after () ->
    providerid = provider.id
    console.log "AFTER"
    console.log provider
    provider.destroy()
      .then (res) ->
        console.log "DESTROYED PROVIDER #{providerid}"

  it 'should not be possible to create a resource without a name', () ->

    # ### Fail Create Test
    # Test data is stored in a [lowdb](https://github.com/typicode/lowdb) database for convenience.  Try creating a bookable without the required name property and confirm that it fails
    test_resource = ld.clone db('resources').find({name: "Test Resource One"}).value()
    delete test_resource.name
    console.log test_resource
    bk = new Resource(provider.body.subdomain)
    tr0 = bk.create test_resource
    tr0.should.eventually.not.have.property "id"
    
  it 'should be possible to create a resource from a body object', () ->
    # ### Create Test
    # Test data is stored in a [lowdb](https://github.com/typicode/lowdb) database for convenience.  Retrieve a test resource, create it and confirm that it has been created and assigned default properties
    test_resource = ld.clone db('resources').find({name: "Test Resource One"}).value()
    console.log test_resource
    rs = new Resource(provider.body.subdomain)
    tr0 = rs.create test_resource
      .then (bkb) ->
        resource = bkb
    tr0.should.eventually.have.property "id"
    tr0.should.eventually.have.deep.property "body.availability"
    tr0.should.eventually.have.deep.property "body.description"
    tr0.should.eventually.have.deep.property "body.slug"
    tr0.should.eventually.have.deep.property "body.active", true

  it 'should be possible to retrieve the created resource', () ->
    # ### Retrieve Test
    # Retrieve the resource via the API to confirm that the call is working
    new_resource = new Resource(provider.body.subdomain, resource.id)
    tr1 = new_resource.retrieve()
    tr1.should.eventually.have.deep.property "body.description", resource.body.description
    tr1.should.eventually.have.deep.property "body.slug", resource.body.slug
    tr1.should.eventually.have.deep.property "body.active", resource.body.active
    tr1.should.eventually.satisfy (newobj) ->
      console.log newobj
      ld.isEqual newobj.body.availability, resource.body.availability

  it 'should be possible to modify the created resource', () ->
    # ### Update Test
    # Retrieve the resource via the API to confirm that the call is working
    new_availability_check = ld.clone db('misc').find({key: "availability"}).value()
    new_availability_check = new_availability_check.value
    new_availability = ld.clone resource.body.availability
    new_availability.sunday = new_availability.saturday
    new_availability.saturday = []
    console.log "NEW AVAIL"
    console.log new_availability
    new_name = "Updated #{resource.body.name}"
    nrs = resource.update {name: new_name, availability: new_availability}
      .then (res) ->
        resource.retrieve()
      .then (res) ->
        console.log "UPDATED AVAIL"
        console.log res.body.availability
        resource = res
    nrs.should.eventually.have.deep.property "body.name", new_name
    nrs.should.eventually.have.deep.property "body.availability.saturday.length", new_availability.saturday.length
    
  it 'should be possible to delete the created resource', () ->
    # ### Delete Test 
    # Delete the bookable and then confirm that the list of bookables for the provider is one less than before
    rs = resource.destroy()
      .then (res) ->
        provider.retrieve()
    rs.should.eventually.have.deep.property "body.resources.length", 0
