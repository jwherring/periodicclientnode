# ## Whitelabel
#
# The tests in this file are written for the [mocha](http://visionmedia.github.io/mocha/) testing framework and use [should](https://github.com/visionmedia/should.js/) syntax as provided by the [Chai](http://chaijs.com/) assertion library
chai = require 'chai'
chaiaspromised = require 'chai-as-promised'
should = chai.should()
chai.use chaiaspromised
crypto = require 'crypto'
low = require 'lowdb'
ld = require 'lodash'

# Note that the test data filepath is relative to the project root dir.  It is important to run tests from the project root dir for this reason
db = low('test/db.json')
periodic = require '../periodic.coffee'
# Import the domain and (optionally) port number from `setup.coffee`
config = require './setup'
PeriodicGateway = periodic.PeriodicGateway
Whitelabel = periodic.Whitelabel

describe 'URL Instantiation Test', () ->
  
  endpoint = client = whitelabel = {}

  before () ->
    # The endpoint is the domain name plus an optional port number.  Tests are run under the assumption that this domain points to a running version of the Periodic application
    endpoint = if config.port? then "#{config.domain}:#{config.port}" else "#{config.domain}"
    # ### Client Instantiation
    # Instantiate the client by passing your apikey and the endpoint of the running Periodic application to the `PeriodicGateway` class.  `PeriodicGateway` is a [singleton](http://en.wikipedia.org/wiki/Singleton_pattern), so this only needs to be done once
    client = PeriodicGateway.set(config.adminkey, config.adminname, endpoint)
    # ### Resource Instantiation
    # URL schemas for particular resource are contained in classes named after the resource.
    whitelabel = new Whitelabel()

  # #### URL Schema Test
  # This first test checks that the RESTful urls are correctly generated
  it 'should be able to create a whitelabel class and set up appropriate urls', () ->
    (whitelabel.posturl).should.equal("http://#{endpoint}/whitelabel")
    (whitelabel.indexurl).should.equal("http://#{endpoint}/whitelabel")

    # Resource locators for individual resources are only generated if the schema class is passed a resource id.  Note that this id must be a string.
    (whitelabel).should.not.have.property("idurl")

  it 'should set up an individual resource url if supplied with an id, which must be a string', () ->
    whitelabel = new Whitelabel('1')
    (whitelabel).should.have.property("idurl")
    (whitelabel.idurl).should.equal("http://#{endpoint}/whitelabel/id/1")

    whitelabel_two = new Whitelabel(1)
    (whitelabel_two).should.not.have.property("idurl")

describe 'CREATE test', () ->
  
  endpoint = client = whitelabel = {}

  # Create the endpoint, client and whitelabel for all tests
  before () ->
    endpoint = if config.port? then "#{config.domain}:#{config.port}" else "#{config.domain}"
    client = PeriodicGateway.set(config.adminkey, config.adminname, endpoint)
    whitelabel = new Whitelabel()

  # Clean up the created whitelabel after the test has run
  after () ->
    client.apikey = config.adminkey
    client.apiuser = config.adminname
    resid = whitelabel.id
    dest = whitelabel.destroy()
    dest.then (res) ->
      console.log "DESTROYED WHITELABEL #{resid}"

  it 'should not be able to create a whitelabel with missing required fields', () ->

    # ### Fail Create Test
    # Test data is stored in a [lowdb](https://github.com/typicode/lowdb) database for convenience.  Get the first test whitelabel ahead of making the CREATE call.  These are all the required fields.  Remove some fields to check that it fails
    test_whitelabel_one = ld.clone db('whitelabels').find({name: "Test Whitelabel"}).value()
    delete test_whitelabel_one.name
    wl = new Whitelabel()
    wl0 = wl.create test_whitelabel_one
    wl0.should.eventually.not.have.property "id"

    test_whitelabel_two = ld.clone db('whitelabels').find({name: "Test Whitelabel"}).value()
    delete test_whitelabel_two.url
    wl = new Whitelabel()
    wl1 = wl.create test_whitelabel_two
    wl1.should.eventually.not.have.property "id"

    test_whitelabel_three = ld.clone db('whitelabels').find({name: "Test Whitelabel"}).value()
    delete test_whitelabel_three.label
    wl = new Whitelabel()
    wl2 = wl.create test_whitelabel_three
    wl2.should.eventually.not.have.property "id"

  it 'should not be able to create a whitelabel with provider-level credentials', () ->

    # ### Fail Credential Test
    # Only superadmin users should be able to create whitelabels
    test_whitelabel_one = ld.clone db('whitelabels').find({name: "Test Whitelabel"}).value()

    # Set the client credentials to regular user credentials
    client.apikey = config.userkey
    client.apiuser = config.username

    wl = new Whitelabel()
    
    # Confirm that the API will not create a new whitelabel with these credentials 
    wl0 = wl.create test_whitelabel_one
    wl0.should.eventually.not.have.property "id"
    
    # Set the credentials back to admin-level 
    client.apikey = config.adminkey
    client.apiuser = config.adminname

  it 'should be able to take a body object and create a corresponding whitelabel', () ->
    
    # ### Create Test
    # Test data is stored in a [lowdb](https://github.com/typicode/lowdb) database for convenience.  Get the first test whitelabel ahead of making the CREATE call.
    test_whitelabel_one = ld.clone db('whitelabels').find({name: "Test Whitelabel"}).value()

    # CREATE a whitelabel with this sample data
    wl1 = whitelabel.create test_whitelabel_one

    # Since network calls return a promise, use the [Chai-as-Promised](https://github.com/domenic/chai-as-promised/) `eventually` wrapper to check that the `idurl` and `body.id` data members have been set as a result.  Note that the global provider has to be reassigned in the resolution of the promise in order for it to be available to the next test.
    wl1.then (res) ->
      whitelabel = res
    wl1.should.eventually.have.property "idurl"
    wl1.should.eventually.have.property "body"
    wl1.should.eventually.have.property "id"
    wl1.should.eventually.have.deep.property "body.id"
    wl1.should.eventually.have.deep.property "body.url", test_whitelabel_one.url
    wl1.should.eventually.have.deep.property "body.label", test_whitelabel_one.label
    wl1.should.eventually.have.deep.property "body.name", test_whitelabel_one.name

  it 'should be able to retrieve a newly-created whitelabel', () ->
    
    target = ld.clone db('whitelabels').find({name: "Test Whitelabel"}).value()

    # ### Retrive Test (standard)
    # Instantiate a new whitelabel object with the id of the one we just created and try to populate it with data from the server
    # 
    # Because Whitelabels are typically retrieved by reference to the domain name, and since the Gateway is a singleton, we have to request its instantiation via a class method and modify the endpoint directly.
    instance = PeriodicGateway.getInstance()
    instance.endpoint = "http://#{target.label}.app:#{config.port}/"

    whitelabel_three = new Whitelabel(whitelabel.id)
    wl3 = whitelabel_three.retrieve()
    wl3.then (res) ->
      whitelabel_three = res
    wl3.should.eventually.have.property "body"

    # Check that everything submitted was accepted without modification
    wl3.should.eventually.have.property "id", whitelabel.id
    wl3.should.eventually.have.deep.property "body.name", whitelabel.body.name
    wl3.should.eventually.have.deep.property "body.label", whitelabel.body.label
    wl3.should.eventually.have.deep.property "body.url", whitelabel.body.url

    # ### Retrieve Test (localhost)
    # Instantiate a new whitelabel object with the id of the one we just created and try to populate it with data from the server
    # 
    # Because Whitelabels are typically retrieved by reference to the domain name, we have to make a local call to test the normal GET by id endpoint.  Since the Gateway is a singleton, we have to request its instantiation via a class method and modify the endpoint directly.

    instance = PeriodicGateway.getInstance()
    instance.endpoint = "http://localhost:#{config.port}/"

    whitelabel_three = new Whitelabel(whitelabel.id)
    wl3 = whitelabel_three.retrieve()
    wl3.then (res) ->
      whitelabel_three = res
    wl3.should.eventually.have.property "body"

    # Check that everything submitted was accepted without modification
    wl3.should.eventually.have.property "id", whitelabel.id
    wl3.should.eventually.have.deep.property "body.name", whitelabel.body.name
    wl3.should.eventually.have.deep.property "body.label", whitelabel.body.label
    wl3.should.eventually.have.deep.property "body.url", whitelabel.body.url

    # Reset the endpoint to its original value for the next test
    instance.getEndpoint()

  it 'should be able to update a newly-created whitelabel', () ->
    
    # ### Update Test
    # Update the original whitelabel with some sample data and verify that the information returned from the server is the same as the sample information passed in

    update_whitelabel_one = db('whitelabels').find({name: "Update Test Whitelabel"}).value()
    wl4 = whitelabel.update update_whitelabel_one
    wl4.then (res) ->
      whitelabel = res
    wl4.should.eventually.have.deep.property "body.name", update_whitelabel_one.name

  it 'should not be able to delete a whitelabel with provider credentials', () ->

    # ### Fail Delete Test
    # Set the key and username to the provider-level user to confirm that provider-level users cannot delete whitelabels.  Since success on delete causes the id property to be erased, check that it's still there after the attempt

    client.apikey = config.userkey
    client.apiuser = config.username
    console.log "DESTROY WL"
    console.log whitelabel.id
    test_whitelabel_four = new Whitelabel(whitelabel.id)
    wl5 = test_whitelabel_four.destroy()
    wl5.should.eventually.have.property "id"

