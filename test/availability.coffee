# ## Availability
#
# The tests in this file are written for the [mocha](http://visionmedia.github.io/mocha/) testing framework and use [should](https://github.com/visionmedia/should.js/) syntax as provided by the [Chai](http://chaijs.com/) assertion library
require('mocha-as-promised')()
chai = require 'chai'
chaiaspromised = require 'chai-as-promised'
should = chai.should()
chai.use chaiaspromised
crypto = require 'crypto'
low = require 'lowdb'
ld = require 'lodash'
expected_number_of_resources = 3

# Note that the test data filepath is relative to the project root dir.  It is important to run tests from the project root dir for this reason
db = low('test/db.json')
periodic = require '../periodic.coffee'
# Import the domain and (optionally) port number from `setup.coffee`
config = require './setup'
PeriodicGateway = periodic.PeriodicGateway
Bookable = periodic.Bookable
Provider = periodic.Provider
Reservation = periodic.Reservation
Resource = periodic.Resource
ResourceGroup = periodic.ResourceGroup
Availability = periodic.Availability

describe 'CREATE Test', () ->

  this.timeout(5000)
  
  # The client, endpoint and provider are reused for all tests.
  endpoint = client = provider = provider2 = reservation = resource1 = resource2 = resource3 = resource4 = resource5 = resourcegroup = bookable1 = bookable2 = bookable3 = availability = {}

  tag_one = 'one'
  tag_two = 'two'

  ts_count = 0

  # Create the endpoint, client and provider for all tests
  before () ->
    endpoint = if config.port? then "#{config.domain}:#{config.port}" else "#{config.domain}"
    client = PeriodicGateway.set(config.userkey, config.username, endpoint)
    provider = new Provider()
    provider_body = ld.clone db('providers').find({name: "Test Provider One"}).value()
    provider2_body = ld.clone db('providers').find({name: "Test Provider Two"}).value()
    resource1_body = ld.clone db('resources').find({name: "Test Resource One"}).value()
    resource2_body = ld.clone db('resources').find({name: "Test Resource Two"}).value()
    resource3_body = ld.clone db('resources').find({name: "Test Resource Three"}).value()
    #resource4_body = ld.clone db('resources').find({name: "Test Resource Four"}).value()
    #resource5_body = ld.clone db('resources').find({name: "Test Resource Five"}).value()
    bookable1_body = ld.clone db('bookables').find({name: "Test Bookable One"}).value()
    bookable1_body.duration = 3600
    bookable2_body = ld.clone db('bookables').find({name: "Test Bookable Two"}).value()
    bookable2_body.duration = 3600
    bookable3_body = ld.clone db('bookables').find({name: "Test Bookable Three"}).value()
    provider.create provider_body
      .then (res) ->
        console.log "CREATED PROVIDER: #{provider.id}"
        provider2 = new Provider()
        provider2.create provider2_body
      .then (res) ->
        console.log "CREATED PROVIDER: #{provider2.id}"
        bookable1 = new Bookable(provider.body.subdomain)
        bookable1.create bookable1_body
      .then (res) ->
        bookable2 = new Bookable(provider.body.subdomain)
        bookable2.create bookable2_body
      .then (res) ->
        bookable3 = new Bookable(provider2.body.subdomain)
        bookable3.create bookable3_body
      .then (res) ->
        resource1 = new Resource(provider.body.subdomain)
        resource1.create resource1_body
      .then (res) ->
        resource2 = new Resource(provider.body.subdomain)
        resource2.create resource2_body
      .then (res) ->
        resource3 = new Resource(provider.body.subdomain)
        resource3.create resource3_body
      .then (res) ->
        required_resources = [resource1.id]
        bookable1.update {requiredresources: required_resources}
      .catch (err) ->
        console.log err

  # Clean up the created provider after the test has run.
  after () ->
    resid = provider.id
    res2id = provider2.id
    dest = provider.destroy()
    dest.then (res) ->
      console.log "DESTROYED PROVIDER #{resid}"
      provider2.destroy()
        .then (res) ->
          console.log "DESTROYED PROVIDER #{res2id}"

  it 'should not be possible to get availability with missing required fields', () ->

    # ### Fail Get Test
    # Availability needs start and end times.  Try making an availability call without them.  Since we've set up 
    # resources and bookables on a dummy provider, interface through the provider by setting the provider member
    # variable to the dummy provider's subdomain.  Note that the call used is "search" rather than "retrieve,"
    # as would be the case with other entity types.  This is because availability is a GET-only (retrieve-only) 
    # interface, and relevant parameters are passed in the url rather than through the message body.
    av = new Availability()
    av.provider = provider.body.subdomain
    tav1 = av.search()
    tav1.should.eventually.satisfy (cd) ->
      cd.statuscode isnt 200

  it 'should not be possible to get availability from provider with start, end and no bookable', () ->

    # ### Fail Get Test
    # If we give Availability start and end times and a bookable, we should be able to get a list of timeslots.
    # Without a bookable, it should return 302
    av = new Availability()
    start = ld.clone db('misc').find({key: "start"}).value()
    end = ld.clone db('misc').find({key: "end"}).value()
    av.provider = provider.body.subdomain
    params =
      start: start.value
      end: end.value
    tav1 = av.search params
      .then (res) ->
        res
    tav1.should.eventually.satisfy (cd) ->
      cd.statuscode isnt 200
    tav1.catch (err) ->
      console.log "ERROR IN SEARCH"
      console.log err.message

  it 'should be possible to get availability from provider with start, end and bookable', () ->
    # ### Get Test
    # If we give Availability start and end times and a bookable, we should be able to get a list of timeslots.
    # Furthermore, all 5 resources set up in the before() call should be listed on each timeslot since 
    # we havent booked any appointments yet
    av = new Availability()
    start = ld.clone db('misc').find({key: "start"}).value()
    end = ld.clone db('misc').find({key: "end"}).value()
    av.provider = provider.body.subdomain
    params =
      start: start.value
      end: end.value
      bookable: bookable1.id
    tav1 = av.search params
    tav1.should.eventually.have.property 'statuscode', 200
    tav1.should.eventually.satisfy (cd) ->
      ld.every cd.body, (ts) ->
        ld.has(ts, 'start') and ld.has(ts, 'end') and ld.has(ts, 'resources') and ld.has(ts, 'bookables') and ld.has(ts, 'seats_remaining')
    tav1.should.eventually.satisfy (cd) ->
      ld.every cd.body, (ts) ->
        ts.resources.length is bookable1.body.requiredresources.length #since there are no appointments yet
      
  it 'availability for resourceless bookables should log the number of conflicts', () ->
    # ### Get Test
    # If we give Availability start and end times and a bookable, we should be able to get a list of timeslots.
    # Furthermore, all 5 resources set up in the before() call should be listed on each timeslot since 
    # we havent booked any appointments yet
    av = new Availability()
    start = ld.clone db('misc').find({key: "start"}).value()
    end = ld.clone db('misc').find({key: "end"}).value()
    av.provider = provider.body.subdomain
    params =
      start: start.value
      end: end.value
      bookable: bookable2.id
    tav1 = av.search params
    tav1.should.eventually.have.property 'statuscode', 200
    tav1.should.eventually.satisfy (cd) ->
      ts_count = cd.body.length
      console.log "TSCOUNT: #{ts_count}"
      ld.every cd.body, (ts) ->
        ld.has(ts, 'start') and ld.has(ts, 'end') and ld.has(ts, 'resources') and ld.has(ts, 'bookables') and ld.has(ts, 'seats_remaining')

  it 'availability for resourceless bookables should show one fewer slot after booking an appointment', () ->
    # ### SEARCH Test
    # After booking an appointment, we should show one fewer availability slot than before
    av = new Availability()
    start = ld.clone db('misc').find({key: "start"}).value()
    startplusonehour = ld.clone db('misc').find({key: "startplusonehour"}).value()
    startplustwohours = ld.clone db('misc').find({key: "startplustwohours"}).value()
    end = ld.clone db('misc').find({key: "end"}).value()
    av.provider = provider.body.subdomain
    resourceless_reservation =
      start: startplusonehour.value
      end: startplustwohours.value
      bookable: bookable2.id
    rs = new Reservation(provider.body.subdomain)
    reservation_test = rs.create resourceless_reservation
      .then (res) ->
        params =
          start: start.value
          end: end.value
          bookable: bookable2.id
        av.search params
    reservation_test.should.eventually.satisfy (cd) ->
      console.log "TSCOUNT: #{ts_count}"
      console.log "ACTUAL: #{cd.body.length}"
      console.log cd.body
      cd.body.length is ts_count - 1

  it 'should be possible to pass in resource ids and limit the results', () ->
    start = ld.clone db('misc').find({key: "start"}).value()
    startplusonehour = ld.clone db('misc').find({key: "startplusonehour"}).value()
    end = ld.clone db('misc').find({key: "end"}).value()
    av = new Availability()
    av.provider = provider.body.subdomain
    resourcesstring = "#{resource1.id},#{resource2.id}"
    params =
      start: start.value
      end: end.value
      bookable: bookable1.id
      resources: resourcesstring
    tav = av.search params
      .then (res) ->
        res
    tav.should.eventually.have.property 'statuscode', 200
    tav.should.eventually.satisfy (cd) ->
      ld.every cd.body, (ts) ->
        ts.resources.length <= 2

  it 'should not be possible to get availability by tag before any tags have been set', () ->
    start = ld.clone db('misc').find({key: "start"}).value()
    end = ld.clone db('misc').find({key: "end"}).value()
    startplusonehour = ld.clone db('misc').find({key: "startplusonehour"}).value()
    av = new Availability()
    params =
      start: start.value
      end: end.value
      tags: tag_one
    tav = av.search params
      .then (res) ->
        res
    tav.should.eventually.have.property 'statuscode', 302
    tav.should.eventually.have.property 'errormessage', '"No bookables found for these tags"'

  it 'should be possible to get availability by tag before after tags have been set', () ->
    start = ld.clone db('misc').find({key: "start"}).value()
    end = ld.clone db('misc').find({key: "end"}).value()
    startplusonehour = ld.clone db('misc').find({key: "startplusonehour"}).value()
    av = new Availability()
    params =
      start: start.value
      end: end.value
      tags: tag_one
    tav = bookable1.update {tags: [tag_one]}
      .then (res) ->
        av.search params
      .then (res) ->
        res
    tav.should.eventually.have.property 'statuscode', 200
    tav.should.eventually.have.property 'body'

  it 'should also accept a comma-separated list of tags', () ->
    start = ld.clone db('misc').find({key: "start"}).value()
    end = ld.clone db('misc').find({key: "end"}).value()
    startplusonehour = ld.clone db('misc').find({key: "startplusonehour"}).value()
    av = new Availability()
    params =
      start: start.value
      end: end.value
      tags: "#{tag_one},#{tag_two}"
    tav =  av.search params
        .then (res) ->
          res
    tav.should.eventually.have.property 'statuscode', 200
    tav.should.eventually.have.property 'body'
    tav.should.eventually.satisfy (tv) ->
      ld.every tv.body, (i) ->
        ld.intersection(ld.keys(i.subdomains), i.bookables).length is i.bookables.length
        
  it 'should be able to get availability across two bookables', () ->
    start = ld.clone db('misc').find({key: "start"}).value()
    end = ld.clone db('misc').find({key: "end"}).value()
    startplusonehour = ld.clone db('misc').find({key: "startplusonehour"}).value()
    av = new Availability()
    params =
      start: start.value
      end: end.value
      tags: "#{tag_one},#{tag_two}"
    tav = bookable3.update {tags: [tag_two]}
      .then (res) ->
        av.search params
    tav.should.eventually.have.property 'statuscode', 200
    tav.should.eventually.have.property 'body'
    tav.should.eventually.satisfy (tv) ->
      ld.every tv.body, (i) ->
        ld.intersection(ld.keys(i.subdomains), i.bookables).length is i.bookables.length
