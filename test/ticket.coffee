# ## Ticket
#
# The tests in this file are written for the [mocha](http://visionmedia.github.io/mocha/) testing framework and use [should](https://github.com/visionmedia/should.js/) syntax as provided by the [Chai](http://chaijs.com/) assertion library
chai = require 'chai'
chaiaspromised = require 'chai-as-promised'
should = chai.should()
chai.use chaiaspromised
crypto = require 'crypto'
low = require 'lowdb'
ld = require 'lodash'
#
# Note that the test data filepath is relative to the project root dir.  It is important to run tests from the project root dir for this reason
db = low('test/db.json')
periodic = require '../periodic.coffee'
# Import the domain and (optionally) port number from `setup.coffee`
config = require './setup'
PeriodicGateway = periodic.PeriodicGateway
Provider = periodic.Provider
User = periodic.User
Ticket = periodic.Ticket

describe 'URL Instantiation Test', () ->

  endpoint = client = provider = user = ticket = {}

  before () ->
    # The endpoint is the domain name plus an optional port number.  Tests are run under the assumption that this domain points to a running version of the Periodic application
    endpoint = if config.port? then "#{config.domain}:#{config.port}" else "#{config.domain}"
    # ### Client Instantiation
    # Instantiate the client by passing your apikey and the endpoint of the running Periodic application to the `PeriodicGateway` class.  `PeriodicGateway` is a [singleton](http://en.wikipedia.org/wiki/Singleton_pattern), so this only needs to be done once
    client = PeriodicGateway.set(config.userkey, config.username, endpoint)
    # ### Resource Instantiation
    # URL schemas for particular resource are contained in classes named after the resource.
    provider = new Provider()
    provider_body = ld.clone db('providers').find({name: "Test Provider One"}).value()
    provider.create provider_body
      .then (res) ->
        provider = res
        ticket = new Ticket()
        res

  after () ->
    provider.destroy()

  # #### URL Schema Test
  # This first test checks that the RESTful urls are correctly generated
  it 'should be able to create a ticket class and set up appropriate urls', () ->
    (ticket.posturl).should.equal("http://#{endpoint}/ticket")
    (ticket.indexurl).should.equal("http://#{endpoint}/ticket")
    # Resource locators for individual tickets are only generated if the schema class is passed a resource id.  Note that this id must be a string.
    (ticket).should.not.have.property("idurl")

  it 'should set up an individual resource url if supplied with an id, which must be a string', () ->
    # Resource locators for individual tickets are only generated if the schema class is passed both a provider subdomain and a resource id.  This id must be a string.  
    ticket_one = new Ticket('1')
    (ticket_one).should.have.property("idurl")
    (ticket_one.idurl).should.equal("http://#{endpoint}/ticket/id/1")

    ticket_two = new Ticket(1)
    (ticket_two).should.not.have.property("idurl")

describe 'CREATE test', () ->

  endpoint = client = provider = user = ticket = {}

  before () ->
    client = PeriodicGateway.set(config.userkey, config.username, endpoint)
    provider = new Provider()
    provider_body = ld.clone db('providers').find({name: "Test Provider One"}).value()
    provider.create provider_body
      .then (res) ->
        provider = res
        res

  after () ->
    resid = provider.id
    provider.destroy()
      .then (res) ->
        console.log "DESTROYED PROVIDER #{resid}"
        res

  it 'should not be possible to create a ticket without a title', () ->
    tk = new Ticket()
    newtk = tk.create {}
    newtk.should.eventually.not.have.property.id
    newtk.should.eventually.have.property 'errormessage', 'Missing required property: title'

  it 'should be possible to create a ticket with just a title and status should be "open"', () ->
    tk = new Ticket()
    newtk = tk.create {title: "Test Ticket One"}
      .then (res) ->
        ticket = res
        res
    newtk.should.eventually.have.property.id
    newtk.should.eventually.have.deep.property 'body.status', 'open'

  it 'should be possible to retrieve the newly-created ticket by id', () ->
    tk = new Ticket(ticket.id)
    rettk = tk.retrieve()
    rettk.should.eventually.have.deep.property 'body.title', 'Test Ticket One'
    rettk.should.eventually.have.deep.property 'body.status', 'open'

  it 'should be possible to change the title on the ticket', () ->
    tk = new Ticket(ticket.id)
    updatedtk = tk.update {title: "New Title"}
      .then (res) ->
        tk.retrieve()
    updatedtk.should.eventually.have.deep.property 'body.title', 'New Title'

  it 'should be possible to add a message to the ticket', () ->
    tk = new Ticket(ticket.id)
    newmessage =
      subject: "New Message"
      body: "New Message"
    updatedtk = tk.addMessage newmessage
      .then (res) ->
        tk.retrieve()
      .then (res) ->
        user = tk.body.messages[0].sender
        tk
    updatedtk.should.eventually.have.deep.property 'body.title', 'New Title'
    updatedtk.should.eventually.have.deep.property 'body.messages.length', 2

  it 'should be possible to fetch tickets by user', () ->
    tk = new Ticket(ticket.id)
    byuser = tk.byUser user
    byuser.should.eventually.have.deep.property 'body.length', 1
    byuser.should.eventually.have.deep.property 'body[0].title', 'New Title'
    byuser.should.eventually.have.deep.property 'body[0].messages.length', 2

  it 'should be possible to escalate a ticket', () ->
    tk = new Ticket(ticket.id)
    newmessage =
      subject: "Escalate Message"
      body: "Escalate Message"
    escalated = tk.escalate newmessage
    escalated.should.eventually.have.deep.property 'body.title', 'New Title'
    escalated.should.eventually.have.deep.property 'body.messages.length', 3
    escalated.should.eventually.have.deep.property 'body.status', 'open'

  it 'should be possible to close the ticket', () ->
    tk = new Ticket(ticket.id)
    newmessage =
      subject: "Close Message"
      body: "Close Message"
    closed = tk.close newmessage
    closed.should.eventually.have.deep.property 'body.title', 'New Title'
    closed.should.eventually.have.deep.property 'body.messages.length', 4
    closed.should.eventually.have.deep.property 'body.status', 'closed'

  it 'should be possible to delete the created ticket', () ->
    tk = new Ticket(ticket.id)
    deleted = tk.destroy()
    deleted.should.eventually.not.have.property.id
