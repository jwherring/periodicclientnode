# ## Bookable
#
# The tests in this file are written for the [mocha](http://visionmedia.github.io/mocha/) testing framework and use [should](https://github.com/visionmedia/should.js/) syntax as provided by the [Chai](http://chaijs.com/) assertion library
chai = require 'chai'
chaiaspromised = require 'chai-as-promised'
should = chai.should()
chai.use chaiaspromised
crypto = require 'crypto'
low = require 'lowdb'
ld = require 'lodash'

# Note that the test data filepath is relative to the project root dir.  It is important to run tests from the project root dir for this reason
db = low('test/db.json')
periodic = require '../periodic.coffee'
# Import the domain and (optionally) port number from `setup.coffee`
config = require './setup'
PeriodicGateway = periodic.PeriodicGateway
Provider = periodic.Provider
User = periodic.User
Bookable = periodic.Bookable

describe 'URL Instantiation Test', () ->
  
  endpoint = client = provider = user = bookable = {}

  before () ->
    # The endpoint is the domain name plus an optional port number.  Tests are run under the assumption that this domain points to a running version of the Periodic application
    endpoint = if config.port? then "#{config.domain}:#{config.port}" else "#{config.domain}"
    # ### Client Instantiation
    # Instantiate the client by passing your apikey and the endpoint of the running Periodic application to the `PeriodicGateway` class.  `PeriodicGateway` is a [singleton](http://en.wikipedia.org/wiki/Singleton_pattern), so this only needs to be done once
    client = PeriodicGateway.set(config.userkey, config.username, endpoint)
    # ### Resource Instantiation
    # URL schemas for particular resource are contained in classes named after the resource.
    provider = new Provider()
    provider_body = ld.clone db('providers').find({name: "Test Provider One"}).value()
    provider.create provider_body
      .then (res) ->
        provider = res
        bookable = new Bookable(provider.body.subdomain)

  after () ->
    provider.destroy()

  # #### URL Schema Test
  # This first test checks that the RESTful urls are correctly generated
  it 'should be able to create a bookable class and set up appropriate urls', () ->
    (bookable.posturl).should.equal("http://#{endpoint}/bookable")
    (bookable.indexurl).should.equal("http://#{endpoint}/bookable")

    # Resource locators for individual resources are only generated if the schema class is passed a resource id.  Note that this id must be a string.
    (bookable).should.not.have.property("idurl")

  it 'should set up an individual resource url if supplied with an id, which must be a string', () ->
    # Resource locators for individual bookables are only generated if the schema class is passed both a provider subdomain and a resource id.  This id must be a string.  The provider subdomain exists to associate the bookable with a provider, since all bookables belong to providers.  It does not make sense to have a bookable without a provider.
    bookable_one = new Bookable(provider.body.subdomain, '1')
    (bookable_one).should.have.property("idurl")
    (bookable_one.idurl).should.equal("http://#{endpoint}/bookable/id/1")

    bookable_two = new Bookable(provider.body.subdomain, 1)
    (bookable_two).should.not.have.property("idurl")

  it 'should fail to set up an individual resource url if not supplied with a provider subdomain, as Bookables depend on individual providers', () ->
    # Since bookables depend on providers, it is not possible to create a resource locator for an individual bookable without saying which provider it belongs to.  Passing in only a resource id, which works for standalone entities like providers and whitelabels, will not work for bookables.
    bookable_one = new Bookable('1')
    bookable_one.should.not.have.property("idurl")

describe 'CREATE test', () ->

  endpoint = client = provider = user = bookable = {}

  # Create the endpoint, client, provider and user for all tests
  before () ->
    console.log "BEFORE"
    provider_body = ld.clone db('providers').find({name: "Test Provider One"}).value()
    endpoint = if config.port? then "#{config.domain}:#{config.port}" else "#{config.domain}"
    client = PeriodicGateway.set(config.userkey, config.username, endpoint)
    provider = new Provider()
    provider.create provider_body
      .then (res) ->
        provider = res
        console.log "CREATED PROVIDER #{provider.id}"

  # Clean up the created provider and user after the test has run
  after () ->
    providerid = provider.id
    console.log "AFTER"
    console.log provider
    provider.destroy()
      .then (res) ->
        console.log "DESTROYED PROVIDER #{providerid}"

  it 'should not be possible to create a bookable without a name', () ->

    # ### Fail Create Test
    # Test data is stored in a [lowdb](https://github.com/typicode/lowdb) database for convenience.  Try creating a bookable without the required name property and confirm that it fails
    test_bookable = ld.clone db('bookables').find({name: "Test Bookable One"}).value()
    delete test_bookable.name
    console.log test_bookable
    bk = new Bookable(provider.body.subdomain)
    tb0 = bk.create test_bookable
    tb0.should.eventually.not.have.property "id"
    
  it 'should be possible to create a bookable from a body object', () ->
    # ### Create Test
    # Test data is stored in a [lowdb](https://github.com/typicode/lowdb) database for convenience.  Retrieve a test bookable, create it and confirm that it has been created and assigned default properties
    test_bookable = ld.clone db('bookables').find({name: "Test Bookable One"}).value()
    console.log test_bookable
    bk = new Bookable(provider.body.subdomain)
    tb0 = bk.create test_bookable
      .then (bkb) ->
        bookable = bkb
    tb0.should.eventually.have.property "id"
    tb0.should.eventually.have.deep.property "body.availability"
    tb0.should.eventually.have.deep.property "body.notifications"
    tb0.should.eventually.have.deep.property "body.questions"
    tb0.should.eventually.have.deep.property "body.seats"
    tb0.should.eventually.have.deep.property "body.seatsperbooking"
    tb0.should.eventually.have.deep.property "body.tags"

  it 'should be possible to retrieve the created bookable', () ->
    # ### Retrieve Test
    # Retrieve the bookable via the API to confirm that the call is working
    new_bookable = new Bookable(provider.body.subdomain, bookable.id)
    tb1 = new_bookable.retrieve()
    tb1.should.eventually.have.deep.property "body.notifications.length", bookable.body.notifications.length
    tb1.should.eventually.have.deep.property "body.questions.length", bookable.body.questions.length
    tb1.should.eventually.have.deep.property "body.seats", bookable.body.seats
    tb1.should.eventually.have.deep.property "body.seatsperbooking", bookable.body.seatsperbooking
    tb1.should.eventually.satisfy (bkb) ->
      ld.isEqual bkb.body.availability, bookable.body.availability

  it 'should be possible to retrieve the created bookable without a provider subdomain', () ->
    # ### Retrieve Test
    # Retrieve the bookable via the API wihtout a provider subdomain to confirm that the bookable is uniquely identifiable
    new_bookable = new Bookable(null, bookable.id)
    tb1 = new_bookable.retrieve()
    tb1.should.eventually.have.deep.property "body.notifications.length", bookable.body.notifications.length
    tb1.should.eventually.have.deep.property "body.questions.length", bookable.body.questions.length
    tb1.should.eventually.have.deep.property "body.seats", bookable.body.seats
    tb1.should.eventually.have.deep.property "body.seatsperbooking", bookable.body.seatsperbooking
    tb1.should.eventually.satisfy (bkb) ->
      ld.isEqual bkb.body.availability, bookable.body.availability

  it 'should be possible to modify the created bookable', () ->
    # ### Update Test
    # Retrieve the bookable via the API to confirm that the call is working
    #new_availability_check = ld.clone db('misc').find({key: "availability"}).value()
    #new_availability_check = new_availability_check.value
    new_availability = ld.clone bookable.body.availability
    new_availability.sunday = new_availability.saturday
    new_availability.saturday = []
    console.log "NEW AVAIL"
    console.log new_availability
    new_name = "Updated #{bookable.body.name}"
    nbk = bookable.update {name: new_name, availability: new_availability}
      .then (res) ->
        bookable.retrieve()
      .then (res) ->
        console.log "UPDATED AVAIL"
        console.log res.body.availability
        bookable = res
    nbk.should.eventually.have.deep.property "body.name", new_name
    nbk.should.eventually.have.deep.property "body.availability.saturday.length", new_availability.saturday.length
    
  it 'should be possible to delete the created bookable', () ->
    # ### Delete Test 
    # Delete the bookable and then confirm that the list of bookables for the provider is one less than before
    bkb = bookable.destroy()
      .then (res) ->
        provider.retrieve()
    bkb.should.eventually.have.deep.property "body.reservationtypes.length", 0
