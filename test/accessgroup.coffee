# ## AccessGroup
#
# The tests in this file are written for the [mocha](http://visionmedia.github.io/mocha/) testing framework and use [should](https://github.com/visionmedia/should.js/) syntax as provided by the [Chai](http://chaijs.com/) assertion library
chai = require 'chai'
chaiaspromised = require 'chai-as-promised'
should = chai.should()
chai.use chaiaspromised
crypto = require 'crypto'
low = require 'lowdb'
ld = require 'lodash'
#
# Note that the test data filepath is relative to the project root dir.  It is important to run tests from the project root dir for this reason
db = low('test/db.json')
periodic = require '../periodic.coffee'
# Import the domain and (optionally) port number from `setup.coffee`
config = require './setup'
PeriodicGateway = periodic.PeriodicGateway
Provider = periodic.Provider
User = periodic.User
AccessGroup = periodic.AccessGroup

describe 'URL Instantiation Test', () ->
  
  endpoint = client = provider = user1 = user2 = accessgroup = bogususergroup = {}

  before () ->
    # The endpoint is the domain name plus an optional port number.  Tests are run under the assumption that this domain points to a running version of the Periodic application
    endpoint = if config.port? then "#{config.domain}:#{config.port}" else "#{config.domain}"
    # ### Client Instantiation
    # Instantiate the client by passing your apikey and the endpoint of the running Periodic application to the `PeriodicGateway` class.  `PeriodicGateway` is a [singleton](http://en.wikipedia.org/wiki/Singleton_pattern), so this only needs to be done once
    client = PeriodicGateway.set(config.userkey, config.username, endpoint)
    # ### Resource Instantiation
    # URL schemas for particular resource are contained in classes named after the resource.
    accessgroup = new AccessGroup()

  # #### URL Schema Test
  # This first test checks that the RESTful urls are correctly generated
  it 'should be able to create an accessgroup class and set up appropriate urls', () ->
    (accessgroup.posturl).should.equal("http://#{endpoint}/accessgroup")
    (accessgroup.indexurl).should.equal("http://#{endpoint}/accessgroup")

    # Resource locators for individual resources are only generated if the schema class is passed a resource id.  Note that this id must be a string.
    (accessgroup).should.not.have.property("idurl")

  it 'should set up an individual resource url if supplied with an id, which must be a string', () ->
    ag_one = new AccessGroup('1')
    (ag_one).should.have.property("idurl")
    (ag_one.idurl).should.equal("http://#{endpoint}/accessgroup/id/1")

    ag_two = new AccessGroup(1)
    (ag_two).should.not.have.property("idurl")

describe 'CREATE test', () ->

  endpoint = client = provider = user1 = user2 = accessgroup = bogususergroup = {}

  # Create the endpoint, client, provider and user for all tests
  before () ->
    provider_body = ld.clone db('providers').find({name: "Test Provider One"}).value()
    user1_body = ld.clone db('users').find({lastname: "One"}).value()
    user2_body = ld.clone db('users').find({lastname: "Two"}).value()
    endpoint = if config.port? then "#{config.domain}:#{config.port}" else "#{config.domain}"
    client = PeriodicGateway.set(config.userkey, config.username, endpoint)
    provider = new Provider()
    provider.create provider_body
      .then (res) ->
        user1 = new User()
        user1.provider = provider.body.subdomain
        user1.create user1_body
      .then (res) ->
        user1 = res
        user2 = new User()
        user2.provider = provider.body.subdomain
        user2.create user2_body
      .then (res) ->
        user2 = res


  # Clean up the created provider and user after the test has run
  after () ->
    providerid = provider.id
    user1id = user1.id
    user2id = user2.id
    bogusid = bogususergroup.id
    dest = provider.destroy()
    dest.then (res) ->
      console.log "DESTROYED PROVIDER #{providerid}"
      console.log "DESTROYED USER #{user1id}"
      console.log "DESTROYED USER #{user2id}"
      bogususergroup.destroy()
    .then (res) ->
      console.log "DESTROYED BOGUS USER GROUP #{bogusid}"

  it 'should not be possible to create an accessgroup with missing required fields', () ->

    # ### Fail Create Test
    # Test data is stored in a [lowdb](https://github.com/typicode/lowdb) database for convenience.  Get the first test provider ahead of making the CREATE call.  These are all the required fields.  Remove some fields to check that it fails
    test_accessgroup = {}
    ag = new AccessGroup()
    ag0 = ag.create test_accessgroup
    ag0.should.eventually.not.have.property "id"

  it 'should be possible to create an empty accessgroup with only a name', () ->

    # ### Create Test
    # Confirm that passing a name in the body to a POST call returns an empty access group
    test_accessgroup = {name: "Test Name"}
    accessgroup = new AccessGroup()
    ag0 = accessgroup.create test_accessgroup
    ag0.should.eventually.have.property "id"
    ag0.should.eventually.have.deep.property "body.users.length", 0

  it 'should not be possible to create an accessgroup with bogus users', () ->

    # ### Fail Create Test
    # Make sure it is impossible to create an accessgroup with a bogus user
    test_accessgroup = {name: "Test Name", users: ['bogus']}
    bogususergroup = new AccessGroup()
    ag0 = bogususergroup.create test_accessgroup
    ag0.should.eventually.have.property "id"
    ag0.should.eventually.have.deep.property "body.users.length", 0


  it 'should be possible to add a user to the accessgroup', () ->

    # ### Update Test
    # Confirm that adding a user to the group results in a new user being added to the group
    accessgroup.body.users.push user1.id
    ag0 = accessgroup.update()
    ag0.should.eventually.have.property "id"
    ag0.should.eventually.have.deep.property "body.users.length", 1

  it 'should not be possible to add a user who does not exist to the accessgroup', () ->

    # ### FAIL Update Test
    # Confirm that adding a bogus user to the group does not result in a new user being added to the group
    accessgroup.body.users.push "bogus"
    ag0 = accessgroup.update {users: accessgroup.body.users}
    ag0.should.eventually.have.property "id"
    ag0.should.eventually.have.deep.property "body.users.length", 1

  it 'should be possible to retrieve an accessgroup by user', () ->
    # ### GET Test - by user
    # Confirm that we can get a list of accessgroups to which a user belongs
    ag = new AccessGroup(accessgroup.id)
    ag0 = ag.byUser user1.id
    ag0.should.eventually.have.deep.property "body[0].id", accessgroup.id
   
  it 'should be possible to delete the accessgroup', () ->
    # ### Delete Test
    # Confirm that adding a bogus user to the group does not result in a new user being added to the group
    ag0 = accessgroup.destroy()
    ag0.should.eventually.not.have.property "id"
