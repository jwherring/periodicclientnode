ld = require 'lodash'
crypto = require 'crypto'
Promise = require 'bluebird'
Client = require('node-rest-client').Client
request = Promise.promisify require 'request'

checktype = (obj) ->
  if obj == undefined or obj == null
    return String obj
  classToType = {
    '[object Boolean]': 'boolean',
    '[object Number]': 'number',
    '[object String]': 'string',
    '[object Function]': 'function',
    '[object Array]': 'array',
    '[object Date]': 'date',
    '[object RegExp]': 'regexp',
    '[object Object]': 'object'
  }
  classToType[Object.prototype.toString.call(obj)]

class PeriodicGateway

  instance = null

  class Instantiation
    constructor: (@apikey, @apiuser, @base_endpoint='periodicapp.com', skip=2) ->
      components = @base_endpoint.split '.'
      @client = new Client()
      @client.postAsync = Promise.promisify @client.post
      if components.length > skip
        @subdomain = components[0]
        @base_endpoint = components[1..].join '.'
      @getEndpoint()

    getEndpoint: () ->
      if @subdomain?
        @endpoint = "http://#{@subdomain}.#{@base_endpoint}/"
      else
        @endpoint = "http://#{@base_endpoint}/"

  @set: (apikey, apiuser, base_endpoint='periodicapp.com') ->
    instance ?= new Instantiation(apikey, apiuser, base_endpoint)
    instance

  @getInstance: () ->
    instance ?= new Instantiation('sandbox', 'sandboxuser', 'http://periodicapp.com')

class APIEntity

  constructor: (arg, @entitytype) ->
    @gateway = PeriodicGateway.getInstance()
    if arguments.length
      switch checktype arg
        when 'string'
          @id = arg
        when 'object'
          @body = arg
    @endpoint = "#{@gateway.endpoint}#{@entitytype}"
    @posturl = @puturl = @indexurl = @endpoint
    if @id?
      @idurl = "#{@endpoint}/id/#{@id}"

  digest: (req) ->
    hmac = crypto.createHmac 'md5', @gateway.apikey
    hmac.update @serialize(req), 'utf8'
    hmac.digest 'hex'

  serialize: (obj) ->
    switch checktype obj
      when 'object'
        keys = ld.keys obj
        keys.sort()
        ("#{k}#{@serialize obj[k]}" for k in keys).join ''
      when 'array'
        ("#{@serialize o}" for o in obj).join ','
      else
        "#{obj}"

  urlserialize: (params) ->
    switch checktype params
      when 'object'
        kys = ld.keys params
        kys.sort()
        ("#{k}/#{@urlserialize(params[k])}" for k in kys).join '/'
      when 'array'
        params.join ';;'
      else
        encodeURIComponent "#{params}"
    
  storeresponse: (data, response) ->
    if response.statusCode is 200
      @body = data
    console.log data
    console.log response.statusCode

  updatewithresponse: (ref, meth) ->
    (response) ->
      if checktype response is 'array'
        response = response[0]
      else if checktype response is not 'object'
        throw new TypeError 'response should be a JSON object'
      ref.statuscode = response.statusCode
      if response.statusCode is 200
        if meth in ['get', 'search', 'index']
          ref.body = JSON.parse response.body
        else
          if not ref.body?
            ref.body = {}
          ld.assign ref.body, response.body
        ref.id = ref.body.id
        if meth isnt 'search'
          ref.idurl = "#{ref.endpoint}/id/#{ref.body.id}"
      else
        ref.errormessage = response.body
        console.log "Invalid status code: #{response.statusCode}"
        console.log "Method was: #{meth}"
        console.log response.body
      ref

  clearonsuccess: (ref) ->
    (response) ->
      console.log "CLEAR ON SUCCESS"
      if checktype response is 'array'
        response = response[0]
      else if checktype response is not 'object'
        throw new TypeError 'response should be a JSON object'
      if response.statusCode is 200
        delete ref.id
        delete ref.body
      else
        console.log "(DELETE) INVALID RESPONSE CODE"
        console.log response.statusCode
      ref

  addProviderHeader: (args) ->
    if @provider?
      args.headers ?= {}
      args.headers['x-periodic-provider'] = @provider

  addAuthHeader: (args, payload) ->
    console.log "AUTH HEADER"
    console.log payload
    args.headers ?= {}
    args.headers['WWW-Authenticate'] = [@gateway.apiuser, @digest payload].join '::'

  addJsonHeader: (args) ->
    args.headers ?= {}
    args.headers['Content-type'] = 'application/json'

  addMarketplaceHeader: (args) ->
    if @marketplace?
      args.headers ?= {}
      args.headers['x-periodic-marketplace'] = @marketplace

  create: (body) ->
    that = @
    body ?= @body
    options =
      uri: @posturl
      json: body
      method: "POST"
    @addProviderHeader(options)
    @addAuthHeader(options, body)
    request options
      .then @updatewithresponse(that, 'create')
      .catch (err) ->
        console.log "ERROR (CREATE):"
        console.log err

  retrieve: () ->
    that = @
    if @idurl?
      args =
        uri: @idurl
        method: "GET"
      @addProviderHeader(args)
      @addJsonHeader(args)
      @addAuthHeader(args, "/#{@entitytype}/id/#{@id}")
      request args
        .then @updatewithresponse(that, 'get')
        .catch (err) ->
          console.log "ERROR (RETRIEVE):"
          console.log err
    else
      console.log "You must construct this object with an id in order to retrieve information about it from the server"

  update: (body) ->
    that = @
    args =
      uri: @idurl
      json: body
      method: 'PUT'
    @addProviderHeader(args)
    @addAuthHeader(args, body)
    request args
      .then @updatewithresponse(that, 'put')
      .catch (err) ->
        console.log "ERROR (UPDATE):"
        console.log err
    
  destroy: () ->
    console.log "DESTROYING ... #{@idurl}"
    that = @
    args =
      uri: @idurl
      method: 'DELETE'
    @addProviderHeader(args)
    @addAuthHeader(args, "/#{@entitytype}/id/#{@id}")
    request args
      .then @clearonsuccess(that, 'delete')
      .catch (err) ->
        console.log "ERROR (DELETE)"
        console.log err

  index: () ->
    args = {}
    that = @
    console.log "INDEX"
    console.log @indexurl
    args =
      uri: @indexurl
      method: 'GET'
    @addProviderHeader(args)
    @addAuthHeader(args, "/#{@entitytype}")
    request args
      .then @updatewithresponse(that, 'index')
      .catch (err) ->
        console.log "ERROR (INDEX):"
        console.log err

  search: (params) ->
    that = @
    searchparams = @urlserialize params
    args =
      uri: "#{@endpoint}/#{searchparams}"
      method: 'GET'
    console.log "SEARCH"
    @addProviderHeader(args)
    console.log args
    #@addAuthHeader(args, body)
    request args
      .then @updatewithresponse(that, 'search')
      .catch (err) ->
        console.log "ERROR (SEARCH):"
        console.log err

class Availability extends APIEntity

  constructor: (arg) ->
    super(arg, 'availability')
    
class BookableTag extends APIEntity

  constructor: (arg) ->
    super(arg, 'tag')

class Provider extends APIEntity

  constructor: (arg) ->
    super(arg, 'provider')
    
class ProviderGroup extends APIEntity

  constructor: (arg) ->
    super(arg, 'providergroup')
    
class Reservation extends APIEntity
  
  constructor: (@provider, arg) ->
    super(arg, 'reservation')

  removeHold: () ->
    that = @
    args =
      uri: "#{@gateway.endpoint}removehold/id/#{@id}"
      method: 'PUT'
    @addProviderHeader(args)
    request args
      .then @updatewithresponse(that, 'get')
      .catch (err) ->
        console.log "ERROR (REMOVEHOLD):"
        console.log err

  cancel: () ->
    that = @
    args =
      uri: "#{@endpoint}/cancel/id/#{@id}"
      method: 'PUT'
    @addProviderHeader(args)
    request args
      .then @updatewithresponse(that, 'get')
      .catch (err) ->
        console.log "ERROR (CANCEL):"
        console.log err


class Ticket extends APIEntity

  constructor: (arg) ->
    console.log "TICKET: #{arg}"
    super(arg, 'ticket')
    @addmessageurl = "#{@endpoint}/addmessage/id/#{@id}"

  addMessage: (body) ->
    that = @
    args =
      uri: @addmessageurl
      json: body
      method: 'PUT'
    @addProviderHeader(args)
    @addAuthHeader(args, body)
    request args
      .then @updatewithresponse(that, 'put')
      .catch (err) ->
        console.log "ERROR (UPDATE):"
        console.log err

  byUser: (userid) ->
    that = @
    args =
      uri: "#{@endpoint}/byuser/id/#{userid}"
      method: 'GET'
    @addProviderHeader(args)
    @addAuthHeader args, "/#{@entitytype}/byuser/id/#{userid}"
    request args
      .then @updatewithresponse(that, 'get')
      .catch (err) ->
        console.log "ERROR (RETRIEVE):"
        console.log err

  escalate: (body) ->
    that = @
    args =
      uri: "#{@endpoint}/escalate/id/#{@id}"
      json: body
      method: 'PUT'
    @addProviderHeader(args)
    @addAuthHeader args, body
    request args
      .then @updatewithresponse(that, 'put')
      .catch (err) ->
        console.log "ERROR (UPDATE):"
        console.log err

  close: (body) ->
    that = @
    args =
      uri: "#{@endpoint}/close/id/#{@id}"
      json: body
      method: 'PUT'
    @addProviderHeader(args)
    @addAuthHeader args, body
    request args
      .then @updatewithresponse(that, 'put')
      .catch (err) ->
        console.log "ERROR (UPDATE):"
        console.log err

class User extends APIEntity
  
  constructor: (arg) ->
    super(arg, 'user')

class Whitelabel extends APIEntity

  constructor: (arg) ->
    super(arg, 'whitelabel')


class Bookable extends APIEntity
  
  constructor: (@provider, arg) ->
    super(arg, 'bookable')

class Blackout extends APIEntity
  
  constructor: (@provider, arg) ->
    super(arg, 'blackout')

class Question extends APIEntity
  
  constructor: (@provider, arg) ->
    super(arg, 'question')

class Resource extends APIEntity
  
  constructor: (@provider, arg) ->
    super(arg, 'resource')

class ResourceGroup extends APIEntity
  
  constructor: (@provider, arg) ->
    super(arg, 'requirementgroup')

class BookableQuestion extends Bookable
  constructor: (@provider, @bookableid, arg) ->
    super(@provider, @bookableid)
    @endpoint = "#{@idurl}/question"
    @idurl = undefined
    @posturl = "#{@endpoint}/create"
    @indexurl = "#{@endpoint}/index"
    switch checktype arg
      when 'object'
        @body = arg
      when 'string'
        @id = arg
        @idurl = "#{@endpoint}/#{@id}"

class AccessGroup extends APIEntity
  
  constructor: (arg) ->
    super(arg, 'accessgroup')

  byUser: (userid) ->
    that = @
    args =
      uri: "#{@endpoint}/byuser/id/#{userid}"
      method: 'GET'
    @addProviderHeader(args)
    @addAuthHeader args, "/#{@entitytype}/byuser/id/#{userid}"
    request args
      .then @updatewithresponse(that, 'get')
      .catch (err) ->
        console.log "ERROR (RETRIEVE):"
        console.log err

module.exports =
  PeriodicGateway:  PeriodicGateway
  Provider:  Provider
  Availability:  Availability
  AccessGroup:  AccessGroup
  Whitelabel:  Whitelabel
  Bookable:  Bookable
  BookableTag:  BookableTag
  Question:  Question
  Reservation:  Reservation
  Resource:  Resource
  ResourceGroup:  ResourceGroup
  Blackout:  Blackout
  BookableQuestion:  BookableQuestion
  Ticket: Ticket
  User: User
